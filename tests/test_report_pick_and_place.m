point_pair = [
    0.2781    9.2635   -0.7047   -2.6224   -0.9980    0.7986;
    6.6308    5.6687    3.3576    0.5497   -1.1845   -2.4000;
    % -7.5815   -1.7458    4.9206   -1.5637   -1.1205    2.5140
    % 8.9988    0.9511   -0.6756    1.9223    1.4915    1.7404
    ];

robot = Robot('simulate', false);
% Speed up the tests
robot.task_vmax_lin = 0.8; % [task space units/s]
robot.task_vmax_ang = 0.8; % [rad/s]
figure(1)
clf;
t = tiledlayout(2, 2, 'TileSpacing', 'None', 'Padding', 'tight');
title(t, 'Test Robot.pick_and_place');
for k = 1:4
    nexttile;
    robot.plot();
end

robot.approach_zoffset = 2;
phases = {'Go to pick', 'Approach pick', 'Depart pick', ... 
'Go to place', 'Approach place', 'Depart place'};

EEpick = point_pair(1, :)
EEplace = point_pair(2, :)

[qq, changeidx] = robot.pick_and_place(EEpick, EEplace);

t.TileSpacing = 'None';
t.Padding = 'tight';

figure(2)
clf;
t = tiledlayout('flow');
nexttile;
title('Joints');
qplot(qq, phases, changeidx);
nexttile;
title('End Effector');
EEplot(robot.joint2task(qq), phases, changeidx);