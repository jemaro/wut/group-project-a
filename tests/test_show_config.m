% Initialize variables
robot = Robot('simulate', false);
figure(1)
clf;
tiledlayout(2, 4, 'TileSpacing', 'tight', 'Padding', 'tight');

% Select joint angles
% q = randPi([1 6]);
q = [-pi/4 -0.1 0 0 0.1 0];
% q = robot.joint_home;
T = robot.dirKinEE(q);

% Select End Effector position and orientation
% EE = [-2.9720   -0.9700    5.3418    2.6811   -2.1536    2.7853];
% EE = [3.5704   -5.6346    3.0890    2.0494   -0.7487    0.8381];
% T = task2matrixT(EE);
% robot.plot(); 

% Obtain all possible joint solutions
for k = 0:7
    flip = mod(k, 2); k = floor(k/2);
    down = mod(k, 2); 
    back = floor(k/2);
    nexttile;
    try
    q = invKinUnique(T, robot.joint_pos, robot.dh, [back down flip]);
    title(sprintf('back=%d    down=%d    flip=%d', back, down, flip));
    subtitle(sprintf('q1=%3.0f  q2=%3.0f  q3=%3.0f\nq4=%3.0f  q5=%3.0f  q6=%3.0f', round(rad2deg(wrapToPi(q(:))), 3, 'significant')));
    r = Robot('name', sprintf('b=%d d=%d f=%d', back, down, flip));
    r.plot(q)
    catch exception
        fprintf('Catched %s config = ', exception.message)
        disp([back down flip])
    end
end