point_pairs = [
    0.4 0 0.11 pi 0 pi/2;
    -0.4 0 0.11 pi 0 pi/2;

    % 0.3 0.3 0.18 pi 0 pi/2;
    % 0.3 -0.1 0.18 pi 0 pi/2;

];
N = size(point_pairs, 1) / 2;

robot = Robot('simulate', true);
figure(1)
clf;
subplot(2, 2, [1, 3]);
title('Test Robot.pick\_and\_place',robot.trajectory_method);
cla;
robot.plot();
qax = subplot(2, 2, 2);
cla(qax);
title('Joints');
EEax = subplot(2, 2, 4);
title('End Effector');
cla(EEax);

phases = {'Go to pick', 'Approach pick', 'Depart pick', ...
    'Go to place', 'Approach place', 'Depart place'};

for n = 1:N
    subplot(2, 2, [1, 3]);
    
    EEpick = point_pairs(2 * n - 1, :)
    EEplace = point_pairs(2 * n, :)
    
    try
        [qq, changeidx] = robot.pick_and_place(EEpick, EEplace);
    catch exception
        robot.finish_simulation
        rethrow(exception)
    end
    
    subplot(2, 2, 2);
    cla(qax);
    qplot(qq, phases, changeidx);
    subplot(2, 2, 4);
    cla(EEax);
    EEplot(robot.joint2task(qq), phases, changeidx);
    pause(0.2);
end

robot.finish_simulation