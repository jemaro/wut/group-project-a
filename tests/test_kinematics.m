function tests = test_kinematics
    tests = functiontests(localfunctions);
end

function test_dirKin_against_rvctools(TestCase)
    % Test our forward kinematic calculus against Peeter Corke's Robotics
    % Toolbox with random input N times.
    
    N = 10;
    % Initialize our robot
    our_robot = Robot('simulate', false);
    % Initialize third-party library robot with the same parameters
    robot_test = our_robot.init_rvctools();

    % Assert they reach the same values
    for n = 1:N
        q = randPi([1 6]);
        Tn = dirKin(q, our_robot.dh);
        T_ours = Tn(:, :, 6);
        T_ours2 = dirKinEE(q, our_robot.dh);
        T_test = double(robot_test.fkine(q));

        assertEqual(TestCase, T_ours, T_test, 'AbsTol', 0.001)
        assertEqual(TestCase, T_ours2, T_test, 'AbsTol', 0.001)
    end

end

function test_invKin_with_random_input(TestCase)

    N = 10;
    % Initialize robot
    robot = Robot('simulate', false);

    for n = 1:N
        % Get transformation matrix from random input
        q = randPi([1 6]);
        Tn = dirKin(q, robot.dh);
        T = Tn(:, :, 6);
        % Obtain all possible joint solutions
        all_q = invKin(T, robot.joint_pos, robot.dh);
        % Check that the initial random input is one of the possible joint
        % solutions
        testResult = ismembertol(all_q, q);
        [M, ~] = max(sum(testResult, 2));
        assert(M == length(q));
    end

end

function test_all_invKinUnique(TestCase)
    % Tests that the function invKinUnique is able to find the adecuqte
    % solution of a random input in any of the possible configurations

    robot = Robot('simulate', false);
    N = 10;
    OK = 0;
    for n=1:N
        q = randPi([1 6]);
        T = dirKinEE(q, robot.dh);
        all_q = NaN(8, 6);
        % Obtain all possible joint solutions
        for back=[0 1]
            for down=[0 1]
                for flip=[0 1]
                    all_q(1 + flip + down*2 + back*4, :) = invKinUnique(T, ... 
                        robot.joint_pos, robot.dh, [back down flip]);
                end
            end
        end
        testResult = ismembertol(all_q, q);
        [M, ~] = max(sum(testResult, 2));
        try
            assert(M == length(q));
        catch exception
            fprintf('q = ');disp(q);
            fprintf('all_q = ');disp(all_q);
            fprintf('testResult = ');disp(testResult);
            fprintf('passed tests = ');disp(OK);
            rethrow(exception)
        end
        OK = OK + 1;
    end
end

function test_getConfig(TestCase)
    % Test that we are able to guess the current orientation of the manipulator

    robot = Robot('simulate', false);
    N = 100;
    for n=1:N
        q = randPi([1 6]);
        T = dirKinEE(q, robot.dh);
        config = getConfig(q, T, robot.dh);
        q_test = invKinUnique(T, robot.joint_pos, robot.dh, config);
        assertEqual(TestCase, q, q_test, 'AbsTol', 0.001);
    end
end

function test_invKinUnique_with_array_input(TestCase)
    robot = Robot('simulate', false);
    N = 10;
    max_attempts = 100;
    for n=1:N
        q = randPi([1 6]);
        T = dirKinEE(q, robot.dh);
        config = getConfig(q, T, robot.dh);
        % Get a second set of joint angles with the same configuration
        attempt = 1;
        while 1
            q_ = randPi([1 6]);
            T_ = dirKinEE(q_, robot.dh);
            config_ = getConfig(q_, T_, robot.dh);
            if isequal(config_, config)
                q = cat(1, q, q_);
                T = cat(3, T, T_);
                break;
            elseif attempt >= max_attempts
                error('Max attempts reached')
            else
                attempt = attempt + 1;
            end
        end
        % Test
        q_test = invKinUnique(T, robot.joint_pos, robot.dh, config);
        assertEqual(TestCase, q, q_test, 'AbsTol', 0.001);
    end
end

function test_matrixT_from_and_to_EE(TestCase)
    N = 10;

    for n = 1:N
        % Test singularities too
        if n == N
            EE = [randi(5, [1 3]) randPi(1) pi/2 randPi(1)];
        elseif n == N-1
            EE = [randi(5, [1 3]) randPi(1) -pi/2 randPi(1)];
        else
            EE = [randi(5, [1 3]) randPi([1 3])];
        end
        T = task2matrixT(EE);
        EE_test = matrixT2task(T);
        T_test = task2matrixT(EE_test);
        assertEqual(TestCase, T, T_test, 'AbsTol', 1e-8);
    end
end

function test_task2matrixT_with_array_input(TestCase)
    N = 10;
    EE = [randi(5, [N 3]) randPi([N 3])];
    T = task2matrixT(EE);
    EE_test = matrixT2task(T);
    T_test = task2matrixT(EE_test);
    assertEqual(TestCase, T, T_test, 'AbsTol', 1e-8);
end

function test_joint_from_and_to_task(TestCase)
    N = 10;
    robot = Robot('simulate', false);

    for n = 1:N
        q = randPi([1 6]);
        EE = robot.joint2task(q);
        q_test = task2joint(EE, q, robot.dh, robot.getConfig(q, EE));
        assertEqual(TestCase, q, q_test, 'AbsTol', 1e-8);
    end
end

% Deprecated in favour of test_taskTrajectory
% ! This test fails sometimes. It could be improved as a test
% function test_joint2task_with_array_input(TestCase)
%     N = 4;
%     robot = Robot('simulate', false);
%     % Build the input. The first point is random, the following poins are build
%     % by adding some random values to the previous one. Like a random
%     % trajectory
%     qq = zeros([N 6]);
%     qq(1, :) = randPi([1 6]);
%     for k=2:N
%         qq(k, :) = 0.1*(rand([1 6]) - 0.5) + qq(k-1, :);
%     end
%     EEs = robot.joint2task(qq);
%     q_previous = 0.01*(rand([1 6]) - 0.5) + qq(1, :);
%     qq_test = robot.task2joint(EEs, q_previous);
%     assertEqual(TestCase, qq, qq_test, 'AbsTol', 1e-8);
% end

function test_validateLimits(TestCase)
    q = [1 1 -1 1 1 1;
         1 1 1 1 1 60;
         3 1 1 1 1 1];
    qlim = [0 0 0 0 0 NaN;
            2 2 2 2 2 NaN];
    % The first and the last joint vectors have an element out of limits
    assertEqual(TestCase, [0; 1; 0], double(validateLimits(q, qlim)))
    % Test no limits allways gives good
    q = [1 1 -1 1 6*pi -20];
    qlim = [NaN NaN NaN NaN NaN NaN;
            NaN NaN NaN NaN NaN NaN];
    assertEqual(TestCase, 1, double(validateLimits(q, qlim)))
end

function test_validateBoundingBox(TestCase)
    EE = [1 1 -1 1 1 1;
         1 1 0.1 1 1 60;
         3 1 1 1 1 1];
    bb = [0 0 0.1; % min
         2 2 2]; % max
    assertEqual(TestCase, [0; 1; 0], double(validateBoundingBox(EE, bb)))
end

% TODO test_invKin_singularities
