function tests = test_trajectory
    tests = functiontests(localfunctions);
end

function test_taskTrajectory(TestCase)
    N = 3;
    max_attempts = 100;
    r = Robot('simulate', false);
    vzero = zeros(1, r.num_links);
    % figure(1)
    % clf;
    % t = tiledlayout(N, 3);
    % title(t,'test taskTrajectory')

    for n = 1:N

        for attempt = 1:max_attempts
            % EEf = [-2.9720   -0.9700    5.3418    2.6811   -2.1536    2.7853];
            EEf = randEE(r.task_bb);

            try
                [qq, ~, startidx, EEtraj] = taskTrajectory(... 
                    r.trajectory_method, r.qlimits, ...
                    r.config, r.joint_pos, vzero, EEf, vzero, ... 
                    r.task_vmax_lin, r.task_vmax_ang, r.timestep, r.dh);
                break;
            catch exception
                if ~(strcmp(exception.identifier, 'Robot:badinput'))
                    rethrow(exception);
                % else
                %     fprintf('%s = ', exception.message);
                %     disp(attempt)
                end
            end

        end

        if attempt >= max_attempts
            rethrow(exception);
        end

        EEtraj_test = r.joint2task(qq(startidx:end, :));
        assertEqual(TestCase, EEtraj, EEtraj_test, 'AbsTol', 0.001);

        % Plot the results
        % nexttile;
        % qplot(qq);
        % nexttile;
        % EEplot(r.joint2task(qq));
        % nexttile;
        % EEplot(EEtraj);

        % Update the robot position for the new iteration
        r.joint_pos = qq(end, :);
        r.config = r.getConfig(r.joint_pos);

    end
end
