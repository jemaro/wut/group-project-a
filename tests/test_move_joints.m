N = 10;
robot = Robot('simulate', false);
figure(1)
clf;
subplot(1,2,1);
title('Test Robot.move\_joints')
cla
robot.plot();
subplot(1,2,2);
title('Cubic Trajectory');
cla
for n=1:N
    while 1
        qf = randPi([1 6]);
        EEf = robot.joint2task(qf);
        if validateLimits(qf, robot.qlimits) ... 
            && validateBoundingBox(EEf, robot.task_bb)
            break;
        end
    end
    subplot(1,2,1);
    qq = robot.move_joints(qf);
    subplot(1,2,2);
    cla
    qplot(qq);
    pause(0.2);
end