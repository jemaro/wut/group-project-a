points = [
    0.4 0 0.11 pi 0 pi / 2;
    -0.4 0 0.11 pi 0 pi / 2;
    ];
N = size(points, 1);
max_attempts = 100;
robot = Robot();
% Speed up the tests
% robot.task_vmax_lin = 0.5; % [task space units/s]
% robot.task_vmax_ang = 0.8; % [rad/s]
figure(1)
clf;
subplot(2, 2, [1, 3]);
title('Test approach');
cla;
robot.plot();
qax = subplot(2, 2, 2);
cla(qax);
title('Joints');
EEax = subplot(2, 2, 4);
title('End Effector');
cla(EEax);

phases = {'Go to pick', 'Approach pick', 'Retract pick'};

for n = 1:N
    subplot(2, 2, [1, 3]);
    % Select random points until one is valid

    EEpick = points(n, :);
    [qq, ~, changeidx] = robot.approachAndRetractTrajectory(EEpick);
    robot.do_trajectory(qq);

    subplot(2, 2, 2);
    cla(qax);
    qplot(qq, phases, changeidx);
    subplot(2, 2, 4);
    cla(EEax);
    EEplot(robot.joint2task(qq), phases, changeidx);
    pause(0.2);
end

robot.finish_simulation
