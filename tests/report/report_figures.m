qq_cubic = load('qq_cubic.mat'); qq_cubic = qq_cubic.qq;
qq_lspb = load('qq_lspb.mat'); qq_lspb = qq_lspb.qq;
changeidx = load('changeidx.mat'); changeidx = changeidx.changeidx;
phases = {'Go to pick', 'Approach pick', 'Depart pick', ... 
'Go to place', 'Approach place', 'Depart place'};
figure(1);
qplot(qq_cubic, phases, changeidx);
figure(2);
qplot(qq_lspb, phases, changeidx);
robot = Robot('simulate', false);
figure(3);
EE_cubic = robot.joint2task(qq_cubic);
EEsplitplot(EE_cubic, phases, changeidx);
figure(4);
EE_lspb = robot.joint2task(qq_lspb);
EEsplitplot(EE_lspb, phases, changeidx);
figure(5);
qplot(qq_lspb-qq_cubic, phases, changeidx);
ylim([-0.1 0.1]);