N = 5;
max_attempts = 100;
robot = Robot('simulate', false);
% Speed up the tests
robot.task_vmax_lin = 0.5; % [task space units/s]
robot.task_vmax_ang = 0.8; % [rad/s]
figure(1)
clf;
subplot(2, 2, [1, 3]);
title('Test Robot.move\_EE')
cla;
robot.plot();
qax = subplot(2, 2, 2);
cla(qax);
title('Joints');
EEax = subplot(2, 2, 4);
title('End Effector');
cla(EEax);

for n = 1:N
    subplot(2, 2, [1, 3]);
    % Select random points until one is valid
    attempt = 0;
    
    while 1
        attempt = attempt + 1;
        EEf = randEE(robot.task_bb);

        try
            [qq, configf] = robot.move_EE(EEf);
            config0 = robot.getConfig(qq(1, :));
            break;
        catch exception

            if ~(strcmp(exception.identifier, 'Robot:badinput'))
                rethrow(exception);
            end

        end

        if attempt > max_attempts
            rethrow(exception);
        end

    end

    subplot(2, 2, 2);
    cla(qax);
    qplot(qq);
    subplot(2, 2, 4);
    cla(EEax);
    EEplot(robot.joint2task(qq));
    pause(0.2);
end
