function run_tests(varargin)
    % RUN_TESTS executes the test scripts from the 'tests' folder that match
    % with the input strings. If there is no input, the function will run all
    % scripts that match 'test_*.m', which shuld be everything.
    %
    % Usage: 
    % run_tests 'kinematics'

    % Initialize library
    startup_lib;

    % Add tests
    FOLDER = 'tests';
    addpath(FOLDER);

    % Adjust input
    if isempty(varargin)
        varargin{1} = 'test_*.m';
    end

    % Accumulate all tests scripts in a cell array
    tests = {};
    for k = 1:length(varargin)
        match = varargin{k};

        if ~startsWith(match, 'test_')
            match = strcat('test_', match);
        end

        if ~endsWith(match, '.m')
            match = strcat(match, '.m');
        end

        % Accumulate all tests
        scripts = dir([FOLDER '/' match]);
        tests = [tests scripts(:).name];
    end

    % Run tests
    runtests(tests);