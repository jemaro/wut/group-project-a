startup_lib;

%% This is the visualization tool, at the moment it is used to see the pose
% of the robot when varying the different angles (Q1->Q6) using the
% sliders.
% A specific position and orientation (x,y,z,gamma,beta,alpha) can be given
% using the End Effector box, but it might generate errors if this point is
% out the robot's workspace or out of the joints limits.
run_visualization(-22, -27, 66, 3.5, 42, 20)
% These points can be fed using the End effector box for a smooth pick and
% place operation
% TEST1
% Pick up point (x ,y, z) = 0.3, 0.3 , 0.18
% Place point (x ,y, z) = 0.3, -0.1 , 0.18
% 
% TEST2 
% Pick up point (x ,y, z) = 0.06, 0.3 , 0.15
% Place point (x ,y, z) = 0.1, -0.2 , 0.15

%% This runs the pick/place trajectory from different points. A set of test points is 
% provided inside the function for more testing. It is enough the
% comment/uncomment the two lines of a (pick,place) pair and run it again.

run_tests('pick_and_place')