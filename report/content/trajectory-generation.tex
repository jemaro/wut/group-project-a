\section{Trajectory generation}
This section will analyze the procedure of trajectory generation for the pick-and-place task of a 6DOF robot manipulator. This will be performed in the task space and in the joint space, using two different techniques, cubic trajectories in the task and joint space, and LSPB (Linear Segments with Parabolic Blends) trapezoidal trajectory in the task and joint space. 

\subsection{Task definition}
The task of picking up an object by a manipulator can be decomposed into various steps in the task space and joint space. Each of the spaces offers the following characteristics:

Task space:
\begin{itemize}
	\item The position of the end effector in the Cartesian space is always known and therefore easier and more intuitive to control. This leads to the task space being appropriate for very precise control
	\item A straight line in the Cartesian space is a straight line in the task space.
	\item Computing the trajectory in the task space means that the IKP needs to be solved for each point in order to compute the joint angles, which might not be efficient for long trajectories.
\end{itemize}

Joint space:
\begin{itemize}
	\item Only two points in the Cartesian space are needed, thus it is only needed to solve the IKP for the starting point and for the end point. 
	\item Trajectories in the joint space will automatically avoid singular positions. 
	\item A straight line in the Cartesian space is a curve in the joint space.
	\item The values of the position and orientation of the end effector are not directly known, and can be obtained using the solution to the DKP.
\end{itemize}


The conclusion from these points is that it will be convenient to do short and precise movements, such as approaching and departing from the manipulated object, using the task space, and use the joint space for longer movements between the two main starting and ending points as it is not compulsory to know where the robot is at each point, and singularities can be avoided on the way. 

Moreover, the different stages for the pick-and-place are based on \autoref{figure:pickplacetask} task should be the following:
\begin{enumerate}
	\item Move from the current position to $P0\; offset$, which is a point offsetted a certain amount from the TCP (Tool Control Point) to avoid the gripper crashing into it when arriving.
	\item Approach in the task space following a straight line  from $P0\; offset$ to $P0$.
	\item (\textit{Not part of the trajectory}) Close gripper and pick the object.
	\item Depart in the task space following a straight line  from $P0$ to $P0\; offset$.
	\item Move from $P0\; offset$ to $P1\; offset$ in the joint space.
	\item Approach in the task space following a straight line from  $P1\; offset$ to $P1$.
	\item (\textit{Not part of the trajectory}) Open gripper and place the object.
	\item Depart in the task space following a straight line from  $P1$ to $P1\; offset$.
\end{enumerate}

\begin{figure}[H]
\centering
    \includegraphics[width=0.5\textwidth]{\subdir/trajectory_drawing.png}
    \caption{Pick-and-place task points}\label{figure:pickplacetask}
\end{figure}

The following subsections will analyze how to obtain the different trajectory types, linear for approach and depart movements, and cubic and trapezoidal for longer paths.

\subsection{Approach and depart trajectory generation}
The approach and depart movements between two points, \autoref{figure:pickplacetask}, need to be solved in the task space in order to have precise control over the position of the end effector. The procedure for this is to subtract an offset distance from the $z$ coordinate of the end effector.

In this project, it was interesting to make the offset retraction as generic as possible. Therefore the unitary vector representing the end effector frame's $z$ axis was obtained using the euler rotation matrix. Then multiplied by the offset distance, it was added to the end effector position to finally get the offsetted position. 

The angles $[\gamma, \beta, \alpha]$ correspond to the orientation of the end effector and so the $z$ versor can be obtained from the third column of the euler rotation $r_3$,  \autoref{eq:eulerrotation}.

After having both points, $P$ and $P\;offset$, the trajectory between them can be computed using either a cubic polynomial or the LSPB, as will be described in the following sections. 

\begin{equation}
r_3=
\begin{bmatrix}
cos(\alpha)sin(\beta)cos(\gamma)+sin(\alpha)sin(\gamma) \\
sin(\alpha)sin(\beta)cos(\gamma)-cos(\alpha)sin(\gamma) \\
cos(\beta)cos(\gamma)
\end{bmatrix} 
\label{eq:eulerrotation}
\end{equation}

\begin{figure}[H]
\centering
    \includegraphics[width=0.3\textwidth]{\subdir/approach.png}
    \caption{Approach and depart movement}\label{figure:pickplacetask}
\end{figure}

\subsection{Cubic trajectory generation}
The cubic trajectory is a popular approach based on a cubic polynomial fitting between two points. It will lead to a smooth velocity profile and linear acceleration. 

This type of trajectory is based on four parameters, corresponding to the position and speed of the initial and final point.

The cubic polynomial has the form of \autoref{eq:cubicform}

\begin{equation}
q(t)=a_0+a_1t^2+a_2*t^3
\label{eq:cubicform}
\end{equation}

With the endpoint constraints $q(t_0)=q_0$, $q(t_f)=q_f$, $\dot{q}(t_0)=\dot{q_0}$, $\dot{q}(t_f)=\dot{q_f}$. In order to find out the values of the polynomial coefficients, a system of four equations can be formed with the constraints and \autoref{eq:cubicform}. 

The solution to the polynomial coefficients is given by \autoref{eq:cubicsolve}.

\begin{equation}
\begin{bmatrix}
a_0 \\ a_1 \\ a_2 \\ a_3
\end{bmatrix} 
=
\begin{bmatrix}
1& t_0&  t_0^2&    t_0^3 \\
 0&  1&  2*t_0&  3*t_0^2 \\
 1& t_f&  t_f^2&    t_f^3 \\
 0&  1&  2*t_f&  3*t_f^2
\end{bmatrix}^{-1}
\begin{bmatrix}
q_0 \\ \dot{q_0} \\ q_1 \\ \dot{q_1}
\end{bmatrix}
\label{eq:cubicsolve}
\end{equation}

The final trajectory for each joint can be obtained after knowing the polynomial coefficients, by evaluating them for all values of $t$.

A sample of a cubic trajectory is shown in \autoref{figure:cubictraj}. 

\begin{figure}[H]
\centering
    \includegraphics[width=0.4\textwidth]{\subdir/cubic.jpg}
    \caption{Sample cubic trajectory}\label{figure:cubictraj}
\end{figure}

\subsection{LSPB trajectory generation}
Another case of trajectory generation is the Linear Segments with Parabolic Blends (LSPB) trajectory. In this case, the trajectory is divided into three sections, the starting and ending are parabolic and depend on a specified blending time $t_b$, which for testing purposes was considered of 0.2s. The intermediate section is linear, setting the acceleration to zero in this period. This results in a linear variation of the velocity in the start and ending of the process, while keeping it constant between these points.

These stages come represented in \autoref{eq:lspbcases}. Depending on the time step it will be fit either in the linear segment or in the parabolic ones. 

\begin{equation}
\begin{cases}
q_0 + \frac{\alpha}{2}t^2 & t_0\leq t\leq t_0+t_b\\
\frac{q_0 + q_1 - vt_1}{2} + vt & t_0+t_b\leq t\leq t_1\\
q_1 - \frac{\alpha}{2}t_1^2 - \frac{\alpha}{2}t^2 + \alpha t_1 & t_f-t_b\leq t\leq t_f\\
 \end{cases}
 \label{eq:lspbcases}
\end{equation}

\autoref{figure:lspbtraj} shows the three steps involved in the generation of an LSPB trajectory.


\begin{figure}[H]
\centering
    \includegraphics[width=0.4\textwidth]{\subdir/lspb.jpg}
    \caption{Sample LSPB trajectory}\label{figure:lspbtraj}
\end{figure}

\subsection{Implementation on the robot and simulation}
The trajectory generation task was implemented for the most generic pick-and-place case, in which an item can be in any point in space and can be placed in any other point. The two approaches were used, cubic and LSPB, giving very similar results. 

A fact to be considered is that in all stages the same technique was used. This means that, when cubic trajectory is selected (in Robot.m class), all the stages will be based on cubic trajectories (approach, depart, and long movements). The same occurs for the LSPB case.

The results are shown in \autoref{figure:cubicee}, \autoref{figure:lspbee} for the end effector coordinates, and in \autoref{figure:cubicjoint} and \autoref{figure:lspbjoint} for the joint values. The difference is presented in \autoref{figure:trajdiff}.

In the end effector coordinate cases, it should be observed how the approach and depart movements should keep a constant orientation in order to not crash with the object. This can be observed in the graphs from the figures in which the angles $\alpha$, $\beta$, and $\gamma$ are kept constant during this movement.

The difference between the two methods illustrated in \autoref{figure:trajdiff} demonstrates a big similarity with errors oscillating in the order of $10^{-2}$, being smaller ($10^{-3}$) in the end effector frame, $\theta_6$. 

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/cubic_EE.jpg}
    \caption{End effector coordinates in a cubic trajectory}\label{figure:cubicee}
\end{figure}

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/lspb_EE.jpg}
    \caption{End effector coordinates in a LSPB trajectory}\label{figure:lspbee}
\end{figure}

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/cubic_joints.jpg}
    \caption{Joint values in a cubic trajectory}\label{figure:cubicjoint}
\end{figure}

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/lspb_joints.jpg}
    \caption{Joint values in a LSPB trajectory}\label{figure:lspbjoint}
\end{figure}

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/cubic_vs_lspb.jpg}
    \caption{Difference between cubic and LSPB trajectories}\label{figure:trajdiff}
\end{figure}

The trajectory presented in the previous figures is visualized in
\autoref{figure:pickAndPlace_manipulator}. The approach and depart movements
are the straight lines in the visualization.
\autoref{figure:pickAndPlace_variables} shows that the End Effector moves with
constant orientation between two positions during the approach and depart
phases.


\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/pickAndPlace_manipulator.png}
    \caption{View of general pick and place case}
    \label{figure:pickAndPlace_manipulator}
\end{figure} 

\begin{figure}[H]
\centering
    \includegraphics[width=\textwidth]{\subdir/pickAndPlace_variables.png}
    \caption{View of general pick and place case}
    \label{figure:pickAndPlace_variables}
\end{figure} 