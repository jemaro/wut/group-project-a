\section{Direct Kinematics Problem}

This section will describe the steps followed to solve the direct kinematics problem of the manipulator, resulting in the homogeneous transformation matrix ${}^0_6\mathcal{T}$ that gives the position and the orientation of the end effector for known joint variables $\theta_i$.

\subsection{Definitions}
For the sake of shorter expressions the abbreviations described in
\autoref{eqn:abbreviations} are taken into account for the rest of the
report.

\begin{center}\begin{equation}
\begin{array}{c}
 cos(\theta_i) = c_i \\
 sin(\theta_i) = s_i \\
 cos(\theta_i+\theta_j) = c_{i+j} \\
 sin(\theta_i+\theta_j) = s_{i+j} \\
\end{array}\label{eqn:abbreviations}\end{equation}\end{center}

Additionally, one can decompose the homogeneous transformation matrix
$\mathcal{T}$ into its subcomponents using the notation found in
\autoref{eqn:Td}.

\begin{doublespace}
\begin{center}\begin{equation}\mathcal{T}_d=\left(
\begin{array}{cccc}
 r_{11} & r_{12} & r_{13} & p_x \\
 r_{21} & r_{22} & r_{23} & p_y \\
 r_{31} & r_{32} & r_{33} & p_z \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)\label{eqn:Td}\end{equation}\end{center}
\end{doublespace}

\subsection{Homogeneous transformations}

The homogeneous transformations for each joint reference frame with respect to the previous one were computed following \autoref{eqn:Teq}, and resulted in  \autoref{eqn:T01}, \autoref{eqn:T12}, \autoref{eqn:T23}, \autoref{eqn:T34}, \autoref{eqn:T45}, \autoref{eqn:T56}. 

\begin{doublespace}
\begin{center}\begin{equation}
{}^{i-1}_i\mathcal{T} = 
\left(
\begin{array}{cccc}
 cos(\theta_i) & -sin(\theta_i) & 0 & a_{i-1}\\
 sin(\theta_i)cos(\alpha_{i-1}) & cos(\theta_i)cos(\alpha_{i-1}) & -sin(\alpha_{i-1}) & -d_isin(\alpha_{i-1})\\
  sin(\theta_i)sin(\alpha_{i-1}) & cos(\theta_i)sin(\alpha_{i-1}) & cos(\alpha_{i-1}) & -d_icos(\alpha_{i-1})\\
  0 & 0 & 0 & 1

\end{array}
\right)\label{eqn:Teq}\end{equation}\end{center}
\end{doublespace}

Objective of the DKP is to resolve homogeneous matrix
${}^0_6\mathcal{T}$ that corresponds to the frame attached to the end-effector knowing the values of joint variables $\theta_i$. 
Using \autoref{eqn:T}, the intermediate homogeneous transformations are calculated
progressively until ${}^0_6\mathcal{T}$, which can be found in
\autoref{eqn:T06}. 

\begin{equation}
    {}^0_n\mathcal{T}=\prod^n_ {i=1}{}^{i-1}_i\mathcal{T}
    \label{eqn:T}
\end{equation}

\begin{equation}
{}^0_1\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_1 & -s_1 & 0 & 0 \\
 s_1 & c_1 & 0 & 0 \\
 0 & 0 & 1 & d_1 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T01}\end{equation}

\begin{equation}
{}^1_2\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_2 & -s_2 & 0 & 0 \\
 0 & 0 & 1 & 0 \\
 -s_2 & -c_2 & 0 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T12}\end{equation}

\begin{equation}
{}^0_2\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_1 c_2 & -c_1 s_2 & -s_1 & 0 \\
 c_2 s_1 & -s_1 s_2 & c_1 & 0 \\
 -s_2 & -c_2 & 0 & d_1 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T02}\end{equation}

\begin{equation}
{}^2_3\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_3 & -s_3 & 0 & a_2 \\
 s_3 & c_3 & 0 & 0 \\
 0 & 0 & 1 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T23}\end{equation}

\begin{equation}
{}^0_3\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_1 c_{\text{2+3}} & -c_1 s_{\text{2+3}} & -s_1 & a_2 c_1 c_2 \\
 s_1 c_{\text{2+3}} & -s_1 s_{\text{2+3}} & c_1 & a_2 c_2 s_1 \\
 -s_{\text{2+3}} & -c_{\text{2+3}} & 0 & d_1-a_2 s_2 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T03}\end{equation}

\begin{equation}
{}^3_4\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_4 & -s_4 & 0 & a_3 \\
 0 & 0 & 1 & d_4 \\
 -s_4 & -c_4 & 0 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T34}\end{equation}

\begin{equation}
{}^0_4\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_1 c_4 c_{\text{2+3}}+s_1 s_4 & c_4 s_1-c_1 s_4 c_{\text{2+3}} & -c_1 s_{\text{2+3}} & c_1 \left(a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2\right) \\
 c_2 c_3 c_4 s_1-c_4 s_2 s_3 s_1-c_1 s_4 & -s_1 s_4 c_{\text{2+3}}-c_1 c_4 & -s_1 s_{\text{2+3}} & s_1 \left(a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2\right) \\
 -c_4 s_{\text{2+3}} & s_4 s_{\text{2+3}} & -c_{\text{2+3}} & -a_3 s_{\text{2+3}}-d_4 c_{\text{2+3}}-a_2 s_2+d_1 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T04}\end{equation}

\begin{equation}
{}^4_5\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_5 & -s_5 & 0 & 0 \\
 0 & 0 & -1 & 0 \\
 s_5 & c_5 & 0 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T45}\end{equation}

\begin{equation}
{}^0_5\mathcal{T} = 
\left(
\begin{array}{llll}
    \begin{aligned}
    &c_1 \left(c_4 c_5 c_{\text{2+3}}-s_5 s_{\text{2+3}}\right) \\
    &+c_5 s_1 s_4
    \end{aligned} & 
    \begin{aligned}
    &-c_1 \left(c_4 s_5 c_{\text{2+3}}+c_5 s_{\text{2+3}}\right) \\
    &-s_1 s_4 s_5
    \end{aligned} & 
    \begin{aligned}
    &c_1 s_4 c_{\text{2+3}} \\
    &-c_4 s_1
    \end{aligned} & 
    \begin{aligned}
    c_1 \left(a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2\right)
    \end{aligned} \\ \\
    \begin{aligned}
    &c_5 \left(c_2 c_3 c_4 s_1-c_4 s_2 s_3 s_1-c_1 s_4\right) \\
    &-s_1 s_5 s_{\text{2+3}}
    \end{aligned} & 
    \begin{aligned}
    &-c_2 c_5 s_1 s_3 \\
    &+s_5 \left(c_4 s_1 s_2 s_3+c_1 s_4\right) \\
    &-c_3 s_1 \left(c_5 s_2+c_2 c_4 s_5\right)
    \end{aligned} & 
    \begin{aligned}
    &s_1 s_4 c_{\text{2+3}} \\
    &+c_1 c_4
    \end{aligned} & 
    \begin{aligned}
    s_1 \left(a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2\right)
    \end{aligned} \\ \\
    \begin{aligned}
    &s_5 \left(-c_{\text{2+3}}\right) \\
    &-c_4 c_5 s_{\text{2+3}}
    \end{aligned} & 
    \begin{aligned}
    &s_2 \left(c_5 s_3+c_3 c_4 s_5\right) \\
    &+c_2 \left(c_4 s_3 s_5-c_3 c_5\right)
    \end{aligned} & 
    \begin{aligned}
    -s_4 s_{\text{2+3}}
    \end{aligned} & 
    \begin{aligned}
    &-a_3 s_{\text{2+3}}-d_4 c_{\text{2+3}}-a_2 s_2 \\
    &+d_1
    \end{aligned} \\ \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T05}\end{equation}

\begin{equation}
{}^5_6\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_6 & -s_6 & 0 & 0 \\
 0 & 0 & 1 & d_6 \\
 -s_6 & -c_6 & 0 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T56}\end{equation}

\begin{equation}
{}^0_6\mathcal{T} = 
\left(
\begin{array}{llll}
    \begin{aligned}
    &c_1 c_{\text{2+3}} \left(c_4 c_5 c_6-s_4 s_6\right) \\
    &-c_1 c_6 s_5 s_{\text{2+3}}\\
    &+s_1 \left(c_5 c_6 s_4+c_4 s_6\right)
    \end{aligned} &
    \begin{aligned}
    &c_6 \left(c_4 s_1-c_1 s_4 c_{\text{2+3}}\right) \\
    &-s_6 c_1 c_4 c_5 c_{\text{2+3}} \\
    &+s_6 c_1 s_5 s_{\text{2+3}} \\
    &-s_6 c_5 s_1 s_4
    \end{aligned} & 
    \begin{aligned}
    &-c_1 \left(c_4 s_5 c_{\text{2+3}}+c_5 s_{\text{2+3}}\right) \\
    &-s_1 s_4 s_5
    \end{aligned} &
    \begin{aligned} 
    &c_1 c_{\text{2+3}} \left(a_3-c_4 d_6 s_5\right) \\
    &-c_1 s_{\text{2+3}} \left(c_5 d_6+d_4\right)\\
    &+c_1 a_2 c_2 \\
    &-d_6 s_1 s_4 s_5 
    \end{aligned} \\ \\
    \begin{aligned}
    &c_6 c_5 c_4 \left(c_2 c_3 s_1-s_2 s_3 s_1\right) \\
    &-c_6 c_5 c_1 s_4 \\
    &-c_6 s_1 s_5 s_{\text{2+3}} \\
    &-s_6 \left(s_1 s_4 c_{\text{2+3}}+c_1 c_4\right)
    \end{aligned} &
    \begin{aligned}
    &s_1 s_6 s_5 s_{\text{2+3}} \\
    &+s_1 s_6 c_4 c_5 s_2 s_3 \\
    &-s_1 s_6 c_2 c_3 c_4 c_5 \\
    &-s_1 c_6 s_4 c_{\text{2+3}} \\
    &+c_1 \left(c_5 s_4 s_6-c_4 c_6\right)
    \end{aligned} &
    \begin{aligned}
    &-c_2 c_5 s_1 s_3 \\
    &+s_5 \left(c_4 s_1 s_2 s_3+c_1 s_4\right) \\ 
    &-c_3 s_1 \left(c_5 s_2+c_2 c_4 s_5\right)
    \end{aligned} & 
    \begin{aligned}
    &c_2 s_1 c_3 \left(a_3-c_4 d_6 s_5\right) \\
    &+c_2 s_1 a_2 \\
    &-c_2 s_1 s_3 \left(c_5 d_6+d_4\right) \\
    &-a_3 s_1 s_3 s_2 \\
    &-c_3 s_1 s_2 \left(c_5 d_6+d_4\right) \\
    &+c_4 d_6 s_1 s_3 s_5 s_2 \\
    &+c_1 d_6 s_4 s_5
    \end{aligned} \\ \\
    \begin{aligned}
    &s_4 s_6 s_{\text{2+3}} \\
    &-c_6 c_3 \left(c_4 c_5 s_2+c_2 s_5\right) \\
    &-c_6 s_3 \left(c_2 c_4 c_5-s_2 s_5\right)
    \end{aligned} & 
    \begin{aligned}
    &c_3 c_6 s_2 s_4 \\
    &+c_3 s_6 \left(c_4 c_5 s_2+c_2 s_5\right) \\
    &+s_3 c_2 \left(c_6 s_4+c_4 c_5 s_6\right) \\
    &-s_3 s_2 s_5 s_6
    \end{aligned} & 
    \begin{aligned}
    &s_2 \left(c_5 s_3+c_3 c_4 s_5\right) \\
    &+c_2 \left(c_4 s_3 s_5-c_3 c_5\right)
    \end{aligned} & 
    \begin{aligned}
    &-c_2 s_3 a_3 \\
    &+c_2 s_3 c_4 d_6 s_5 \\
    &-c_2 c_3 c_5 d_6 \\
    &-c_2 c_3 d_4 \\
    &+s_2 c_3 \left(c_4 d_6 s_5-a_3\right)\\
    &+s_2 s_3 \left(c_5 d_6+d_4\right) \\
    &-s_2 a_2 + d_1
    \end{aligned} \\ \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T06}\end{equation}

\subsection{Implementation}

The solution presented has been implemented in Matlab to test the results of the direct problem solutions. The source code can be found
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/lib/kinematics/dirKin.m}{here}.

Additionally, the following tables show the results of the tests performed for two possible configurations. 
To confirm the accuracy of the results, they were compared to the results
generated by the forward kinematics defined in the open source
\href{https://github.com/petercorke/robotics-toolbox-matlab}{Robotics Toolbox}.
Said test suite can be found in
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/tests/test_kinematics.m}{test\_kinematics.m}
and executed using:

\begin{lstlisting}[language=matlab,label=test-kinematics]
    run_tests 'kinematics'
\end{lstlisting}

It can be observed how the FKP solution matches exactly the solution generated by the Robotics toolbox.

\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|l|l|l|l|l|l|}
    \hline
                            &        & X          & Y          & Z          & $\alpha$   & $\beta$   & $\gamma$    \\ \hline
    \multirow{2}{*}{Test 1} & RVC FK & 2.0244     & 0.7185     & 5.1997     & 2.8494    & -0.1843     & -2.4523    \\ \cline{2-8} 
                            & FKP    & 2.0244     & 0.7185     & 5.1997     & 2.8494    & -0.1843     & -2.4523    \\ \hline
    \end{tabular}
    \caption{Input Q1=0.1847, Q2=1.3296, Q3=-1.9793, Q4=-2.8573, Q5=-2.5026, Q6=-0.2727}
    \label{table:FKPtest1}
\end{table}

\begin{table}[H]
    \centering
    \begin{tabular}{|l|l|l|l|l|l|l|l|}
    \hline
                            &        & X          & Y           & Z          & $\alpha$   & $\beta$   & $\gamma$    \\ \hline
    \multirow{2}{*}{Test 2} & RVC FK & 0.6049     & -0.2799     & -5.4390     & 2.7494    & 0.7565     & -0.4655    \\ \cline{2-8} 
                            & FKP    & 0.6049     & -0.2799     & -5.4390     & 2.7494    & 0.7565     & -0.4655    \\ \hline
    \end{tabular}
    \caption{Input Q1=-0.0797, Q2=1.2873, Q3=1.0392, Q4=2.1252, Q5=-0.1362, Q6=-1.8503}
    \label{table:FKPtest2}
\end{table}
