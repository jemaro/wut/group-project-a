\section{Application guidelines}
This section will help the user interact and correctly use the tool and choose between the different 
implemented modes depending on the complexity of the desired tasks.

\subsection{General structure}
\subsubsection{Interface states}
Before explaining each feature individually, it is beneficial to first have a general idea 
of how all modes/features are wrapped up and how do they interact with each other as illustrated in 
\autoref{fig:interface-states}.

\begin{figure}[h!]
    \centering
    \resizebox{.60\textwidth}{!}{%
    \begin{tikzpicture}[node distance=8em,auto,
    stt/.style={state, text width=4em,align=center},
    ]
        % ? Should "manual", "auto", "pick" and "place" be accepting states ?
        \node (m) [stt,initial right] at (0,3.5) {Manual};
        \node (doconfig) [stt, text width=5.6em] at (-5,2.5) 
            {Changing\\Configuration};
        \node (dohome) [stt] at (0,0) {Moving\\To Home};
        \node (a) [stt] at (-3.5,-2.5) {Auto};
        \node (dotraj) [stt,below of=a] {Doing\\trajectory};
        \node (pick) [stt] at (3.5, -2.5) {Pick};
        \node (place) [stt,below of=pick] {Place};
        \node (dopp) [stt,below of=place] {Doing\\Pick and\\Place};
        
        %% Edges
        \draw 
            % Loops
            (m) edge [above, loop above] node [align=center] 
                {Changed Joints\\$\lor$ Changed EE\\$\lor$ Error} (m)
            (dopp) edge [loop below] node{Loop} (dopp)
            % State connections
            (a) edge [left, bend right] node{Move} (dotraj)
            (dotraj) edge [right, bend right] node{Finished} (a)
            (pick) edge [bend right] node{Select} (place)
            (place) edge [left, bend right] node{Select and Perform} (dopp)
            (dopp) edge [right, bend right=60] 
                node{Finished $\land$ No Loop} (pick)
            (m) edge [above, bend right=40] node [align=center] 
                {Changed back\\$\lor$ Changed down\\$\lor$ Changed flip} (doconfig)
            (doconfig) edge [below, bend right=-20] node [align=right]
                {Finished\\$\land$ previous Manual} (m)
            (a) edge [left, bend left=40] node [align=center]
                {Changed back\\$\lor$ Changed down\\$\lor$ Changed flip} (doconfig)
            (doconfig) edge [right, bend left=-20, near start] node [align=left]
            {Finished\\$\land$ previous Auto} (a)
        ;
        % Mode changes
        \foreach \x/\y in {m/a,a/pick,pick/m} {%
        \draw[gray] 
            (\x) edge [auto=above, bend left=5] node{} (\y)
            (\y) edge [auto=above, bend left=5] node{} (\x)
        ;}
        \draw[gray]
            (place) edge [above, bend right=90] node{} (m)
            (place) edge [above] node{} (a)
        ;
        % Errors
        \draw[red]
            (a) edge [loop left] node{} (a)
            (pick) edge [loop right] node{} (pick)
            (place) edge [loop right] node{} (place)
        ;
        % Connect to Moving Home
        \foreach \k in {m,a,pick} {%
        \draw[blue] 
            (\k) edge [auto=above, bend left=10] node{} (dohome)
            (dohome) edge [auto=above, bend left=10] node{} (\k)
        ;}
        \draw[blue] 
            (place) edge [auto=above, bend left=20] node{} (dohome)
            (dohome) edge [auto=above, bend left=-10] node{} (place)
        ;
    \end{tikzpicture}
    }
    \caption{Overfiew of interface states.}
    \label{fig:interface-states}
\end{figure}

Some connections have been removed the conditions and given 
a colour: \textcolor{red}{Red} connections are conditioned by an error; \textcolor{blue}{Blue} are
connections from a second state to and from \emph{Moving To Home}. 

If the connection is to \emph{Moving To Home}, it is conditioned by pressing the
button \emph{Home}. Otherwise the condition is that the trajectory to the
Home position has finished and the previous state corresponds to the
connected one; Finally, \textcolor{gray}{gray} connections are conditioned
by the push of the corresponding radio button in the \emph{Mode} panel.

\subsubsection{Substates}
Furthermore, the control system can be described as in \autoref{fig:substates}.

\begin{figure}[h!]
    \centering
    \resizebox{.60\textwidth}{!}{%
    \begin{tikzpicture}[node distance=13em,auto,
        stt/.style={state, text width=4em,align=center},
        ]
        \node (nl) [stt,initial] {No Loop};
        \node (l) [stt,right of=nl] {Loop};
        \draw
            (nl) edge [above, bend left] node [align=center] 
                {Pick $\lor$ Place\\$\lor$ Doing Pick and Place} (l)
            (l) edge [below, bend left] node [align=center] 
                {Pick $\lor$ Place\\$\lor$ Doing Pick and Place} (nl)
        ;
        \node (JS) [stt,text width=5em,initial,right of=l] {Joint Space\\Trajectory};
        \node (TS) [stt,text width=5em,right of=JS] {Task Space\\Trajectory};
        \draw
            (JS) edge [above, bend left] 
                node{Auto} (TS)
            (TS) edge [below, bend left] 
                node{Auto} (JS)
        ;
        \node (GO) [stt,initial,below of=l] {Gripper\\Open};
        \node (GC) [stt,right of=GO] {Gripper\\Closed};
        \draw
            (GO) edge [above, bend left] 
                node{Manual $\lor$ Auto} (GC)
            (GC) edge [below, bend left] 
                node{Manual $\lor$ Auto} (GO)
        ;
    \end{tikzpicture}
    }
    \caption{Interface substates.}
    \label{fig:substates}
\end{figure}

Where:
\begin{itemize}
    \item Left top substates \emph{Loop} and \emph{No Loop}
    will affect the \emph{Doing Pick and Place} state,
    \item Right top substates \emph{Joint Space Trajectory} and 
    \emph{Task Space Trajectory} will affect the \emph{Doing Trajectory} state,
    \item Bottom substates \emph{Gripper Open} and
    \emph{Gripper closed} will also be altered during the \emph{Doing Pick and
    Place} state.
\end{itemize}

\subsection{Home position control}
This feature defines a so-called "home position". Such position is considered as safe as the robot is positioned in a neutral manner (e.g. no singularity).

There's a factory home position defined that the user can move the robot to (or view its position). However, one can also set a home position depending on the task's needs.

\subsection{Control mode}
The gripper can operate in three different modes, where each one presents advantages in different use cases. 
One can choose between:
\begin{itemize}
    \item Manual
    \item Automatic trajectory
    \item Pick and Place
\end{itemize}

\subsubsection{Manual}
Operating in the joint space, manual mode allows the user to fully take control and manually adjust the internal variables of the gripper to one's needs. This implies that one can
adjust the robot's link joints value (expressed in degrees) seperately as well as the robot's end effector position and orientation
as the robot is operating in the joint space, there will be no possibility for the gripper to come across singularity.

\subsubsection{Automatic trajectory}
The following mode can operate in two different coordinate system, and thus one can choose between the joint space or the task space, depending on one's needs.
A major advantage of this mode is that the control system only needs two points, where the first point is the starting point while the second one is the desired final points.
However, the behavior of this function differs depending on the configuration space chosen.
Once the task is triggered by pressing the "move" button, the behavior in case of:
\begin{itemize}
    \item Joint space: as mentioned in the previous subsection, there's no case, where singularity will occur as the control system operates on joint values
    and therefore, the robot will perform the movement even if the target point is outside of the working box or is in a non-permitted travel area (e.g. manipulator rotates around its own axis)
    \item Task space: the control system will try to generate a feasible trajectory in the cartesian space. Depending on whether the generated trajectory 
    If the desired end position is feasible, the gripper will perform the task as expected. However, if the opposite case, the program will return an error.
\end{itemize}

\subsubsection{Pick and Place}
Operating in a mixture of joint and task space, Pick and Place mode gives the user the ability to program a certain movement to perform the pick and place task.
Once selected, a coordinate axes frame (acting as a helper object) will appear at the exact position, where the end-effector is currently positioned. 
To succesfully program a task, two actions to be defined are required:
\begin{itemize}
    \item define the pick position: With the help of the coordinate axes frame, the user can easily move it to a specific spot, using the joint angles and the end effector control.
    Once coordinate axes frame is positioned at the desired pick position, the "select pick" button should be pressed to confirm the pick position.

    This will lead to the second required actions, described in the next point.
    \item define the place position: following the same procedure used to define the pick position, one can define the place position.
\end{itemize} 
Upon confirmation, the task will be performed by the gripper, where during the task, the end-effector will turn red to express that the object is currently grabbed by the end
while a blue end-effector means that the object was placed in the place position

\textbf{NOTE.} The "loop movement" feature can be enabled to infinitely repeat the task.

\subsection{Recorded movement}
To analyze and track the robot internal variables during a movement, the control system also keeps track of each joint angle change during the latest movement. 

Once the movement is done and performed, the graph will plot and update itself.

\subsection{Mode selection guide}
As mentioned previously, each mode is selected depending on the complexity of the user's end goal, therefore,
in some cases, it may be beneficial to pick one mode over another.
Concretely, this can be summed up by taking into consideration the following scenarios:
\begin{itemize}
    \item Directly interact with the robot's internal variables $\xrightarrow{\text{mode}}$ manual
    \item Generate an uncontrolled trajectory to a desired end-effector position with avoiding singular cases $\xrightarrow{\text{mode}}$ Automatic in joint space
    \item Generate a controlled trajectory (intuitive motion avoiding crashes) to a desired end-effector position $\xrightarrow{\text{mode}}$ Automatic in task space
    \item Perform a pick and place on a token $\xrightarrow{\text{mode}}$ Pick and place
\end{itemize}