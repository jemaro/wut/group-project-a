\section{Visualization}
\label{sec:visualization}
The objective of the visualization is to build 
and present a working, interactive interface 
that would give the user the ability to visualize
and to understand the robot's motion while 
performing a task.

Typically, many parameters (like the robot joints or the end-effector position)
are constantly updated. 

\subsection{Prerequisites}
The GUI has been developed using MATLAB programming 
language. The interface was created and designed with
the help of the "GUIDE" environment. Therefore, it is
required to have a valid MATLAB version. 

On the other hand, the visualization is based and
achieved by using the "Robotics Toolbox by Peter Corke".
The library is mainly used to create a visual representation of joints
that constitute our 6-DOF robot.
Thus, the toolbox is also needed to successfully use 
the interface. The initialization of the toolbox will take place automatically.

\subsection{How to run}
\subsubsection{Windows}
\begin{itemize}
    \item Through CMD: with the help of "run.bat", this will start a minimized version of MATLAB
    and the interface. 6 parameters are required - Q1, Q2, Q3, Q4, Q5, Q6 for joint1, 2, 3, 4, 5, 6 respectively
    can also be passed to specifiy the initial joint angles that will be used to position the robot.
    \begin{verbatim}
        // Q1=2, Q2=70, Q3=0, Q4=140, Q5=-20 Q6=-10
        run.bat 2 70 0 140 -20 -10
    \end{verbatim}
    \item Through MATLAB: by starting the script "run\_visualization". This can also take optional parameters - 
    Q1, Q2, Q3, Q4, Q5, Q6 for joint1, 2, 3, 4, 5, 6 respectively can also be passed to specifiy 
    the initial joint angles that will be used to position the robot.
    \begin{verbatim}
        // No initial joint angles specified
        run_visualization

        // Q1=2, Q2=70, Q3=120, Q4=70, Q5=-20 Q6=-50
        run_visualization(20, 70, 0, 140, -20, -10)
    \end{verbatim}
\end{itemize}

\subsubsection{Linux \& MacOS}
\begin{itemize}
    \item Through MATLAB: by starting the script "run\_visualization". This can also take optional parameters - 
    Q1, Q2, Q3, Q4, Q5, Q6 for joint1, 2, 3, 4, 5, 6 respectively can also be passed to specifiy 
    the initial joint angles that will be used to position the robot.
    \begin{verbatim}
        // No initial joint angles specified
        run_visualization
        
        // Q1=2, Q2=70, Q3=120, Q4=70, Q5=-20 Q6=-50
        run_visualization(20, 70, 0, 140, -20, -10)
    \end{verbatim}
\end{itemize}

\subsection{Layout}
To enhance the user experience, the interface was designed with the idea to keep
each region of the tool defined for one specific group of informations.

The interface (as shown \autoref{figure:viz-interface}) is divided into three containers. One can find:
\begin{itemize}
    \item Top-left container - all internal variables of the robot 
    \item Bottom-left container - status of the robot and graph over time for
    each joint angle
    \item Bottom container - Motion control features.
    \item Center container - visualization robot figure.
\end{itemize}
\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{\subdir/GUI3.png}
    \caption{Screenshot of the interface}
    \label{figure:viz-interface}
\end{figure}
Features will be later on explained in detail in the next section.

\subsection{Features}
\subsubsection{Robot Joint angles}
Values that represent the actual joint angle for each
joint. On every robot move, an update is triggered. 

\textbf{Remark.} If manual mode is activated, these values are interactive
    and can be changed.

\subsubsection{End-Effector Position}
Values that represent the actual end-effector position
and orientation. On every robot move, an update is triggered.

\textbf{Remark.} If manual mode is activated, these values are interactive
and can be changed.

\subsubsection{Pick \& Place}
Takes 2 sets of XYZ coordinates in the cartesian space, where the first
set of coordinates is the position to where the robot needs to
travel to in order to pick-up the token.

On the other hand, the second set of coordinates is the target, where the token needs to be placed.

After computing the feasibility of the task, the pipeline will either:
\begin{itemize}
\item Perform the task - No errors were encountered. The robot performs the task.
\item Error - in case either the pick or place targets are out of range or 
a singularity is encountered.
\end{itemize}
In order to select the desired pick and place positions, it is only needed to drive the robot manually
to desired location and confirming the pick (or place) position by pressing the approriate button.

\textbf{NOTE:} \\ The \emph{"Loop movement"} toggle button allows to perform the pick and place task continuously , simulating an actual manipulator in the industry.
\subsubsection{Move}
Drives the end-effector to a specific XYZ point with a specified set of 
gamma/Beta/Alpha orientation.

\subsubsection{Home Position}
Displays the state of the currently defined home position in cartesian space.
One can also define the current position of the robot as the home position.

\subsubsection{Manual Mode}
When activated, enables the user to control manually the robot's 
configuration.

\subsection{Process Flow}
When the GUI is started, external libraries are 
initialized and loaded in:
\begin{itemize}
    \item Robotics toolbox - this will place the correct 
    directories in your MATLAB path 
    \item Robot class - consists of the 6-axis robot
    definition and plenty helper functions used to drive the
    robot, generate trajectory...
\end{itemize}
Once done, the GUI initializes an instance of the robot class,
 defines links accordingly to the DH parameters and finally 
gathers them as a set of links (using RVC toolbox) that 
will constitute the core of the visualization. A visual robot 
mannequin is displayed on the main figure of the interface
and is positioned in the so-called "home position". This position
is defined in the joint space thus, each joint has a default
predifined joint angle assigned to it. This allows one
to make sure that the robot will not encounter singularity during
the travel path from a random starting point.

One is now able to interact and drive the robot to a specific
point defined by point. 

On every move request, the pipeline is triggered by generating
a new trajectory with the last robot's position as an input to
finally drive the robot along the generated path to its final
target.

\autoref{figure:viz-flow-diagram} shows an overall look of the 
process.
\begin{figure}[h]
    \centering
    \includegraphics[width=1.0\textwidth]{\subdir/flow_diagram.png}
    \caption{Block diagram demonstrating the flux of process}
    \label{figure:viz-flow-diagram}
\end{figure}