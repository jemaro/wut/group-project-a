\section{Inverse Kinematic Problem}

The objective of the inverse kinematics problem (IKP) is to determine the joint variables $\theta_i$ for a known homogeneous matrix ${}^0_6\mathcal{T}$. Subsequently, the result of solving the IKP for the solution obtained from the DKP should result in the same angles $\theta_i$ that were used as input for the manipulator arm.

\autoref{eqn:Td06} will the starting point for the solution of the Inverse
Kinematic Problem. The left side of the equation is the solution of the Direct
Kinematic Problem described in \autoref{eqn:T06}. The right side of the
equation is the decomposed homogeneous transformation defined by
\autoref{eqn:Td} which hosts the parameters given as known data for the
resolution of the Inverse Kinematic Problem, the position and orientation of
the end effector.

\begin{equation}
{}^0_6\mathcal{T} = \mathcal{T}_d
\label{eqn:Td06}\end{equation}

By manipulating this equation using intermediate homogeneous transformations,
one can isolate the unknown axis angles from each other and solve them as a
function of the given parameters.

\subsection{Definitions}

\autoref{eqn:Tinverse} defines the nomenclature that will be used for inverse
homogeneous transformations.

\begin{equation}
    {}^x_y\mathcal{T}={}^y_x\mathcal{T}^{-1}
    \label{eqn:Tinverse}
\end{equation}

It is also necessary to define the inverse of the homogeneous transformation matrix, $\mathcal{T}^{-1}$ shown in \autoref{eqn:tau} and \autoref{eqn:tau_inv}.

\begin{equation}
\mathcal{T} = 
\left(
\begin{array}{cccc}
 n_x & o_x & a_x & p_x \\
 n_y & o_y & a_x & p_y \\
 n_z & o_z & a_x & p_z \\
 0 & 0 & 0 & 1
\end{array}
\right)
\label{eqn:tau}\end{equation}

\begin{equation}
\mathcal{T}^{-1} = 
\left(
\begin{array}{cccc}
 n_x & n_y & a_x n_z & -n·p \\
 o_x & o_y & a_x o_z & -o·p \\
 a_x & a_y & a_x a_z & -a·p \\
 0 & 0 & 0 & 1
\end{array}
\right)
\label{eqn:tau_inv}\end{equation}

Furthermore, the function $\arctan\!2(x, y)$ defines the arc tangent of two numbers, or
four-quadrant inverse tangent. $\arctan\!2(x, y)$ returns the arc tangent of
the two numbers $x$ and $y$. It is similar to calculating the arc tangent of
$\frac{y}{x}$, except that the signs of both arguments are used to determine
the quadrant of the result.

\subsection{Solution of joint 1}
\label{subsec:sol-axis-1}


The intermediate homogeneous transformations \autoref{eqn:T15},
\autoref{eqn:T01} and \autoref{eqn:T56} will be used in order to isolate the
value of $\theta_1$, together with equality defined in \autoref{eqn:Td15}. Said
equation is a result of the manipulation of \autoref{eqn:Td06} and makes use of
the inversion nomenclature defined in \autoref{eqn:Tinverse}.

\begin{equation}
{}^1_5\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_4 c_5 c_{\text{2+3}}-s_5 s_{\text{2+3}} & s_3 \left(c_4 s_2 s_5-c_2 c_5\right)-c_3 \left(c_5 s_2+c_2 c_4 s_5\right) & s_4 c_{\text{2+3}} & a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2 \\
 -c_5 s_4 & s_4 s_5 & c_4 & 0 \\
 s_5 \left(-c_{\text{2+3}}\right)-c_4 c_5 s_{\text{2+3}} & s_2 \left(c_5 s_3+c_3 c_4 s_5\right)+c_2 \left(c_4 s_3 s_5-c_3 c_5\right) & -s_4 s_{\text{2+3}} & -a_3 s_{\text{2+3}}+d_4 \left(-c_{\text{2+3}}\right)-a_2 s_2 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T15}\end{equation}

\begin{equation}
{}^1_5\mathcal{T} = {}^1_0\mathcal{T}\ \mathcal{T}_d\ {}^6_5\mathcal{T}
\label{eqn:Td15}\end{equation}

From all the components of the homogeneous transformation, the second row and
forth column will be extracted for further manipulation. As \autoref{eqn:T1524}
implies. Which results in \autoref{eqn:eq11}.

\begin{equation}
{}^1_5\mathcal{T}_{24} = {}^1_5\mathcal{T}_{d 24}
\label{eqn:T1524}\end{equation}
\begin{equation}
0=-d_6 \left(c_1 r_{23}-s_1 r_{13}\right)+c_1 p_y-s_1 p_x
\label{eqn:eq11}\end{equation}

\autoref{eqn:eq12} is found manipulating \autoref{eqn:eq11}. Grouping the terms multiplying $s_1$ and dividing both sides of the equation by them one can reach the conditional system of equations of \autoref{eqn:eqs13}.

\begin{equation}
c_1 d_6 r_{23}-c_1 p_y=d_6 s_1 r_{13}-s_1 p_x
\label{eqn:eq12}\end{equation}

Further manipulation of \autoref{eqn:eqs13} leads to \autoref{eqn:eqs14}, whose
solution can be found in \autoref{eqn:solAxis1}. If the case in which 
$d_6 r_{13}-p_x=0\land d_6 r_{23}-p_y=0$ is fulfilled, a shoulder singularity configuration will occur that will be analyzed later in the report.

\begin{equation}
\begin{cases}
 c_1 d_6 r_{23}-c_1 p_y=0 & d_6 r_{13}-p_x=0 \\
 \frac{c_1 p_y-c_1 d_6 r_{23}}{p_x-d_6 r_{13}}=s_1 & d_6 r_{13}-p_x\neq 0
\end{cases}
\label{eqn:eqs13}\end{equation}
\begin{equation}
\begin{cases}
 c_1=0 & d_6 r_{13}-p_x=0\land d_6 r_{23}-p_y\neq 0 \\
 \frac{p_y-d_6 r_{23}}{p_x-d_6 r_{13}}=\tan \left(\theta _1\right) & d_6 r_{13}-p_x\neq 0
\end{cases}
\label{eqn:eqs14}\end{equation}
\begin{equation}
\theta _1=\arctan\!2\left(p_x-d_6 r_{13},p_y-d_6 r_{23}\right)
\label{eqn:solAxis1}\end{equation}

%==========COMMENTED OUT=============================
\iffalse
\subsubsection{Shoulder singularity}\label{shoulder-singularity}

In \autoref{subsec:sol-axis-1}, a solution is found for $\theta_1$ in all
scenarios except when $d_6 r_{13}-p_x=0\land d_6 r_{23}-p_y = 0$. This scenario is
represented by \autoref{eqn:eqs1II1}. Which leads to \autoref{eqn:rel11II}.

\begin{equation}
\begin{cases}
d_6 r_{13}-p_x=0\\
d_6 r_{23}-p_y=0
\end{cases}
\label{eqn:eqs1II1}\end{equation}
\begin{equation}
\frac{p_x}{p_y}=\frac{r_{13}}{r_{23}}
\label{eqn:rel11II}\end{equation}

Three joints are coplanar and parallel, so that the three joints
instantaneously permit only rotation about one axis and translation
perpendicular to the common plane—an elbow singularity. Assuming that $\theta_6
\neq 0$, the only three joints that can be coplanar and parallel are joints 2,
3 and 5.

Joints 2 and 3 are always coplanar with the origin of coordinates, and their
plane is actually fixed by $\theta_1$, being a possible plane equation described in \autoref{eqn:elbow-sing-plane}

\begin{equation}
    \frac{s_1}{c_1}*X-Y=0
    \label{eqn:elbow-sing-plane}
\end{equation}
%====================================================
\fi

\subsection{Solution of joint 2}
\label{subsec:sol-axis-2}

Using the same procedure as with the first axis, the homogeneous
transformations \autoref{eqn:T14}, \autoref{eqn:T46} and \autoref{eqn:T01}
related by \autoref{eqn:Td14} will be used to isolate the value of $\theta_2$.

\begin{equation}
{}^1_4\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_4 c_{\text{2+3}} & s_4 \left(-c_{\text{2+3}}\right) & -s_{\text{2+3}} & a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2 \\
 -s_4 & -c_4 & 0 & 0 \\
 -c_4 s_{\text{2+3}} & s_4 s_{\text{2+3}} & -c_{\text{2+3}} & -a_3 s_{\text{2+3}}+d_4 \left(-c_{\text{2+3}}\right)-a_2 s_2 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T14}\end{equation}

\begin{equation}
{}^4_6\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_5 c_6 & -c_5 s_6 & -s_5 & -d_6 s_5 \\
 s_6 & c_6 & 0 & 0 \\
 c_6 s_5 & -s_5 s_6 & c_5 & c_5 d_6 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T46}\end{equation}

\begin{equation}
{}^1_4\mathcal{T} = {}^1_0\mathcal{T}\ \mathcal{T}_d\ {}^6_4\mathcal{T}
\label{eqn:Td14}\end{equation}

The first row and forth column of the matrices on both sides of the relation is
extracted into \autoref{eqn:eq21}. The right side of that equation just depends
on known parameters and $\theta_1$, variable $K$ is introduced as defined in
\autoref{eqn:rel21} in order to shorten and make clearer further development.
\autoref{eqn:eq2I} is the result of using Kin \autoref{eqn:eq21}.


\begin{equation}
{}^1_4\mathcal{T}_{14} = {}^1_4\mathcal{T}_{d14}
\label{eqn:T1414}\end{equation}
\begin{equation}
a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2=-d_6 \left(c_1 r_{13}+s_1 r_{23}\right)+c_1 p_x+s_1 p_y
\label{eqn:eq21}\end{equation}
\begin{equation}
K=c_1\left(p_x- d_6 r_{13}\right)+s_1\left(p_y- d_6 r_{23}\right)
\label{eqn:rel21}\end{equation}
\begin{equation}
a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2=K
\label{eqn:eq2I}\end{equation}

Similarly, the component $34$ of \autoref{eqn:Td14} is extracted into
\autoref{eqn:eq22}. Variable $L$ is introduced as defined in
\autoref{eqn:rel22} and \autoref{eqn:eq2II} is obtained with it.

\begin{equation}
{}^1_4\mathcal{T}_{34} = {}^1_4\mathcal{T}_{d34}
\label{eqn:T1434}\end{equation}
\begin{equation}
a_3 s_{\text{2+3}}+d_4 c_{\text{2+3}}+a_2 s_2=d_6 r_{33}+d_1-p_z
\label{eqn:eq22}\end{equation}
\begin{equation}
L=d_6 r_{33}+d_1-p_z
\label{eqn:rel22}\end{equation}
\begin{equation}
a_3 s_{\text{2+3}}+d_4 c_{\text{2+3}}+a_2 s_2=L
\label{eqn:eq2II}\end{equation}

\autoref{eqn:eq2I} and \autoref{eqn:eq2II} are combined into
\autoref{eqn:eqs21}. Which are manipulated into \autoref{eqn:eqs22}.
\autoref{eqn:eqs23} is found by applying a power of $2$ to both sides of the
previous equations.

\begin{equation}
\begin{cases}
a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}+a_2 c_2=K\\
a_3 s_{\text{2+3}}+d_4 c_{\text{2+3}}+a_2 s_2=L
\end{cases}
\label{eqn:eqs21}\end{equation}
\begin{equation}
\begin{cases}
a_3 c_{\text{2+3}}-d_4 s_{\text{2+3}}=K-a_2 c_2\\
a_3 s_{\text{2+3}}+d_4 c_{\text{2+3}}=L-a_2 s_2
\end{cases}
\label{eqn:eqs22}\end{equation}
\begin{equation}
\begin{cases}
-2 a_3 d_4 c_{\text{2+3}} s_{\text{2+3}}+a_3^2 c_{\text{2+3}}^2+d_4^2 s_{\text{2+3}}^2=-2 a_2 c_2 K+a_2^2 c_2^2+K^2\\
2 a_3 d_4 c_{\text{2+3}} s_{\text{2+3}}+a_3^2 s_{\text{2+3}}^2+d_4^2 c_{\text{2+3}}^2=-2 a_2 L s_2+a_2^2 s_2^2+L^2
\end{cases}
\label{eqn:eqs23}\end{equation}

The combination of both equations in \autoref{eqn:eqs23} leads to
\autoref{eqn:eq23}. Which can be reordered into \autoref{eqn:eq24}. At this
step, the assumption of $a_2\neq 0$ is made. This is a reasonable assumption as
in the case of the distance $a_2=0$, two joints would be combined in the same
axis leading to a loss of one degree of freedom. Therefore, \autoref{eqn:eq25}
is obtained dividing both sides of \autoref{eqn:eq24} by $a_2$.

\begin{equation}
a_2^2+K^2+L^2=2 a_2 c_2 K+2 a_2 L s_2+a_3^2+d_4^2
\label{eqn:eq23}\end{equation}
\begin{equation}
a_2^2-a_3^2-d_4^2+K^2+L^2=2 a_2 c_2 K+2 a_2 L s_2
\label{eqn:eq24}\end{equation}
\begin{equation}
\frac{a_2^2-a_3^2-d_4^2+K^2+L^2}{2 a_2}=c_2 K+L s_2
\label{eqn:eq25}\end{equation}

Variable $M$ is introduced as described in \autoref{eqn:rel23}. Applied in
\autoref{eqn:eq25}, leads to \autoref{eqn:eq26}.

\begin{equation}
M=\frac{a_2^2-a_3^2-d_4^2+K^2+L^2}{2 a_2}
\label{eqn:rel23}\end{equation}
\begin{equation}
M=c_2 K+L s_2
\label{eqn:eq26}\end{equation}

\autoref{eqn:eq26} can be solved by means of the variable change described by
\autoref{eqn:varchange2}, which leads to the relation described by
\autoref{eqn:rel24}. When applied, it leads to \autoref{eqn:eq27}, undoing the
change one can reach \autoref{eqn:eq28}. Which can be ultimately solved into
\autoref{eqn:solAxis2} taking into account the double value of the $\arccos$
function. The solution is complete as long as the value of $K$ is known, which
depends on $\theta_1$, and $K$ or $L$ are different from $0$.

\begin{equation}\begin{split}
r &= \sqrt{K^2 + L^2} \\
\tan(\phi) &= \frac{L}{K}
\end{split}\label{eqn:varchange2}\end{equation}
\begin{equation}
\begin{cases}
L=r \sin (\phi )\\
K=r \cos (\phi )
\end{cases}
\label{eqn:rel24}\end{equation}
\begin{equation}
M=r \cos \left(\phi -\theta _2\right)
\label{eqn:eq27}\end{equation}
\begin{equation}
\arccos\left(\frac{M}{K^2+L^2}\right)=\arctan\!2(K,L)-\theta _2
\label{eqn:eq28}\end{equation}
\begin{equation}
\begin{cases}
\theta _2=\arctan\!2(K,L)-\arccos\left(\frac{M}{K^2+L^2}\right)\\
\theta _2=\arctan\!2(K,L)+\arccos\left(\frac{M}{K^2+L^2}\right)
\end{cases}
\label{eqn:solAxis2}\end{equation}

\subsection{Shoulder singularity}

One can find in the previous section that variable $K$ is the same as one side of the equations in case of singularity from \autoref{eqn:singcases}. Therefore, in case of $K=0$ a singular configuration is obtained. Moreover, it is important to consider that $K$ no longer depends on $\theta_1$, resulting in the possibility of always solving $\theta_2$.

Furthermore, as a result of the shoulder singularity, the inverse kinematics have infinite solutions for $\theta_1$. As stated in \cite{Mecademic2019}, it is physically possible to cross a shoulder singularity while following a line, but at the singularity, the end-effector remains motionless while some of the joints rotate.

\subsection{Solution of joint 3}
\label{subsec:sol-axis-3}

The intermediate homogeneous transformations \autoref{eqn:T25},
\autoref{eqn:T02} and \autoref{eqn:T56} related by \autoref{eqn:Td25} will be
used to isolate the value of $\theta_3$.

\begin{equation}
{}^2_5\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_3 c_4 c_5-s_3 s_5 & -c_5 s_3-c_3 c_4 s_5 & c_3 s_4 & a_3 c_3+a_2-d_4 s_3 \\
 c_4 c_5 s_3+c_3 s_5 & c_3 c_5-c_4 s_3 s_5 & s_3 s_4 & a_3 s_3+c_3 d_4 \\
 -c_5 s_4 & s_4 s_5 & c_4 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T25}\end{equation}

\begin{equation}
{}^2_5\mathcal{T} = {}^2_0\mathcal{T}\ \mathcal{T}_d\ {}^6_5\mathcal{T}
\label{eqn:Td25}\end{equation}
% Variable $A$ is introduced as defined in \autoref{eqn:rel31} and
% \autoref{eqn:eq32} is obtained with it.

In order to obtain $\theta_3$, two steps will be followed. 

First, the element 24 will be extracted, identifying the variables $L$ and $K$ in them. 
% \begin{equation}
% {}^2_5\mathcal{T}_{24}
% \label{eqn:T2524}\end{equation}
% \begin{equation}
% a_3 s_3+c_3 d_4=-d_6 \left(-c_1 s_2 r_{13}-s_1 s_2 r_{23}-c_2 r_{33}\right)+c_2 d_1-c_1 s_2 p_x-c_2 p_z-s_1 s_2 p_y
% \label{eqn:eq31}\end{equation}
% \begin{equation}
% A=s_2 \left(c_1 \left(d_6 r_{13}-p_x\right)+s_1 \left(d_6 r_{23}-p_y\right)\right)+c_2 \left(d_6 r_{33}+d_1-p_z\right)
% \label{eqn:rel31}\end{equation}
% \begin{equation}
% a_3 s_3+c_3 d_4=A
% \label{eqn:eq32}\end{equation}

% \autoref{eqn:eq32} can be solved into \autoref{eqn:solAxis3}. This solution is
% complete as long as the value of $A$ is known, which depends on $\theta_1$ and
% $\theta_2$.

\begin{equation}
{}^2_5\mathcal{T}_{24}={}^2_5\mathcal{T}_{d24}
\label{eqn:T2524}\end{equation}
\begin{equation}
a_3 s_3+c_3 d_4=s_2 \left(c_1 \left(d_6 r_{13}-p_x\right)+s_1 \left(d_6 r_{23}-p_y\right)\right)+c_2 \left(d_6 r_{33}+d_1-p_z\right)
\label{eqn:eq3I}\end{equation}
\begin{equation}
a_3 s_3+c_3 d_4=c_2 L-K s_2
\label{eqn:eq3I2}\end{equation}

Second, using the element 22, variables $L$ and $K$ will be identified again, which will simplify the manipulation.

\begin{equation}
{}^2_5\mathcal{T}_{22}
\label{eqn:T2522}\end{equation}
\begin{equation}
a_3 c_3+a_2-d_4 s_3=c_1 c_2 \left(p_x-d_6 r_{13}\right)+c_2 s_1 \left(p_y-d_6 r_{23}\right)+s_2 \left(d_6 r_{33}+d_1-p_z\right)
\label{eqn:eq3II}\end{equation}
\begin{equation}
a_3 c_3-d_4 s_3=-a_2+c_2 K+L s_2
\label{eqn:eq3II2}\end{equation}

These two steps will result in the system of equations given by \autoref{eqn:eqs32}. A variable change to cylindrical coordinates in $a_3$ and $d_4$ will lead to a simplification of the system of equations. Now, dividing the bottom equation over the top one, and combining it with the same division in \autoref{eqn:eqs32} results in \autoref{eqn:eq34}, that can be used to solve $\theta_3$, \autoref{eqn:solAxis3}.

\begin{equation}
\begin{cases}
a_3 s_3+c_3 d_4=c_2 L-K s_2\\
a_3 c_3-d_4 s_3=-a_2+c_2 K+L s_2
\end{cases}
\label{eqn:eqs32}\end{equation}
\begin{equation}
\begin{cases}
a_3=r \sin (\phi )\\
d_4=r \cos (\phi )
\end{cases}
\label{eqn:rel32}\end{equation}
\begin{equation}
\begin{cases}
r \cos \left(\phi -\theta _3\right)=c_2 L-K s_2\\
r \sin \left(\phi -\theta _3\right)=-a_2+c_2 K+L s_2
\end{cases}
\label{eqn:eqs33}\end{equation}
\begin{equation}
\arctan\!2\left(d_4,a_3\right)-\theta _3=\arctan\!2\left(c_2 L-K s_2,-a_2+c_2 K+L s_2\right)
\label{eqn:eq34}\end{equation}
\begin{equation}
\theta _3=\arctan\!2\left(d_4,a_3\right)-\arctan\!2\left(c_2 L-K s_2,-a_2+c_2 K+L s_2\right)
\label{eqn:solAxis3}\end{equation}

\subsection{Elbow singularity}

One can observe in the previous section that the equation $a_3 s_3+c_3 d_4$ appears in \autoref{eqn:eqs32}, and it is one of the singularity cases given in \autoref{eqn:singcases}. This refers to the fully stretched configuration, and it only influences the computation of the angle $\theta_3$, but it can still be calculated. When computing the solution, as confirmed by \cite{Mecademic}, two sets of inverse kinematic solutions degenerate into a single one.

\subsection{Solution of joint 5}
\label{subsec:sol-axis-5}

At this point the value of $\theta_1$, $\theta_2$ and $\theta_3$ is known. The
intermediate homogeneous transformations \autoref{eqn:T35}, \autoref{eqn:T03}
and \autoref{eqn:T56} related by \autoref{eqn:Td35} will be used to isolate the
value of $\theta_5$.

\begin{equation}
{}^3_5\mathcal{T} = 
\left(
\begin{array}{cccc}
 c_4 c_5 & -c_4 s_5 & s_4 & a_3 \\
 s_5 & c_5 & 0 & d_4 \\
 -c_5 s_4 & s_4 s_5 & c_4 & 0 \\
 0 & 0 & 0 & 1 \\
\end{array}
\right)
\label{eqn:T35}\end{equation}

\begin{equation}
{}^3_5\mathcal{T} = {}^3_0\mathcal{T}\ \mathcal{T}_d\ {}^6_5\mathcal{T}
\label{eqn:Td35}\end{equation}

Component $22$ of \autoref{eqn:Td25} is extracted into \autoref{eqn:eq51}.
Variable $B$ is introduced as defined in \autoref{eqn:rel51} and
\autoref{eqn:eq52} is obtained with it.

\begin{equation}
{}^3_5\mathcal{T}_{22}={}^3_5\mathcal{T}_{d22}
\label{eqn:T3522}\end{equation}
\begin{equation}
c_5=-c_1 r_{13} s_{\text{2+3}}-s_1 s_{\text{2+3}} r_{23}-c_{\text{2+3}} r_{33}
\label{eqn:eq51}\end{equation}
\begin{equation}
A=c_1 r_{13} + s_1 r_{23}
\label{eqn:rel51}\end{equation}
\begin{equation}
c_5=-c_{2+3} r_{33}-s_{2+3} A
\label{eqn:eq52}\end{equation}

\autoref{eqn:eq52} can be solved into \autoref{eqn:solAxis5}. This solution is
complete as long as the value of $A$ (which depends on $\theta_1$), $\theta_2$
and $\theta_3$ is known.

\begin{equation}
\begin{cases}
\theta _5=\arccos\left(-c_{2+3} r_{33}-s_{2+3} A\right)\\
\theta _5=-\arccos\left(-c_{2+3} r_{33}-s_{2+3} A\right)
\end{cases}
\label{eqn:solAxis5}\end{equation}

\subsection{Solution of joint 4}

The intermediate homogeneous transformations \autoref{eqn:T15},
\autoref{eqn:T01} and \autoref{eqn:T56} related by \autoref{eqn:Td15} will be
used to isolate the value of $\theta_4$. The variables $A$ and $B$ are also identified in the system of equations.

\begin{equation}
\begin{cases}
{}^3_5\mathcal{T}_{32}={}^3_5\mathcal{T}_{d32} \\
{}^3_5\mathcal{T}_{12}={}^3_5\mathcal{T}_{d12}
\end{cases}
\label{eqn:T3532and12}\end{equation}

\begin{equation}
\begin{cases}
s_4 s_5=c_1 r_{23}-s_1 r_{13}\\
c_4 s_5=-c_1 r_{13} c_{\text{2+3}}-s_1 c_{\text{2+3}} r_{23}+s_{\text{2+3}} r_{33}
\end{cases}
\label{eqn:eqs4I}\end{equation}

\begin{equation}
B=c_1 r_{23}-s_1 r_{13}
\label{eqn:rel4I}\end{equation}

\begin{equation}
\begin{cases}
s_4 s_5 = B\\
c_4 s_5 = s_{2+3} r_{33} - c_{2+3} A
\end{cases}
\label{eqn:eqs4I2}\end{equation}

An assumption has to be made at this point. Recalling the second singularity case from \autoref{eqn:singcases}, a degree of freedom is lost in the case where $\theta_5$ aligns the axis $4$ and $6$, $s_5=0$. This case will be analyzed in further sections. The assumption to continue calculating $\theta_4$ is $s_5\neq 0$.

% \begin{equation}
% s_4=s_5{}^{-1} \left(c_1 r_{23}-s_1 r_{13}\right)
% \label{eqn:eq4I2}\end{equation}

% Variable $X$ is introduced as defined in \autoref{eqn:rel4I1} and
% \autoref{eqn:eq4I3} is obtained with it. Which can be solved into
% \autoref{eqn:solAxis4I}. This solution is not complete as it does apply only in
% the case where \autoref{eqn:singularity} is false.

After some manipulation of the system of equations, the solution for $\theta_4$ for the case in which $s_5\neq 0$ is given by \autoref{eqn:solAxis4I}. 

\begin{equation}
\theta _4= \arctan\!2\left(s_{2+3} r_{33} - c_{2+3} A, B\right)
\label{eqn:solAxis4I}\end{equation}

\subsection{Solution of joint 6}

The intermediate homogeneous transformations \autoref{eqn:T56} and
\autoref{eqn:T05} related by \autoref{eqn:Td56} will be
used to isolate the value of $\theta_6$. 
\begin{equation}
{}^5_6\mathcal{T} = {}^5_0\mathcal{T}\ \mathcal{T}_d\
\label{eqn:Td56}\end{equation}

Component $11$ of \autoref{eqn:Td56} is extracted into \autoref{eqn:eq6I1} and
component $12$ of \autoref{eqn:Td56} is extracted into \autoref{eqn:eq6I2}.
Which can be used to solve the value of $\theta_6$ as described in
\autoref{eqn:solAxis6I}. This solution is not complete as it does apply only in
the case a non singular wrist configuration.

\begin{equation}
{}^5_6\mathcal{T}_{32}={}^5_6\mathcal{T}_{d11}
\label{eqn:T5611}\end{equation}
\begin{equation}\begin{split}
c_6 &=r_{11} \left(c_5 s_1 s_4+c_1 \left(c_2 \left(c_3 c_4 c_5-s_3 s_5\right)-s_2
\left(c_4 c_5 s_3+c_3 s_5\right)\right)\right)\\
& -r_{21} \left(c_4 c_5 s_1 s_2 s_3+c_1 c_5 s_4+c_3 s_1 s_2 s_5+c_2 s_1 \left(s_3 s_5-c_3 c_4 c_5\right)\right)\\
& +r_{31} \left(s_3 \left(s_2 s_5-c_2 c_4 c_5\right)-c_3 \left(c_4 c_5 s_2+c_2 s_5\right)\right)
\end{split}\label{eqn:eq6I1}\end{equation}
\begin{equation}
{}^5_6\mathcal{T}_{32}={}^5_6\mathcal{T}_{d12}
\label{eqn:T5612}\end{equation}
\begin{equation}\begin{split}
-s_6 &=r_{12} \left(c_5 s_1 s_4+c_1 \left(c_2 \left(c_3 c_4 c_5-s_3 s_5\right)-s_2 \left(c_4 c_5 s_3+c_3 s_5\right)\right)\right)\\
& -r_{22} \left(c_4 c_5 s_1 s_2 s_3+c_1 c_5 s_4+c_3 s_1 s_2 s_5+c_2 s_1 \left(s_3 s_5-c_3 c_4 c_5\right)\right)\\
& +r_{32} \left(s_3 \left(s_2 s_5-c_2 c_4 c_5\right)-c_3 \left(c_4 c_5 s_2+c_2 s_5\right)\right)
\end{split}\label{eqn:eq6I2}\end{equation}

\begin{equation}
\theta _6=\arctan\!2(c_6, s_6)
\label{eqn:solAxis6I}\end{equation}

\subsection{Wrist singularity}
\label{subsec:sol46Sing}

In order to have a complete set of solutions, the value of $\theta_4$ and
$\theta_6$ must be solved in the singular scenario where $s_5 = 0$.

The intermediate homogeneous transformation matrix ${}^1_6\mathcal{T}$ will not
be shown due to its size. It can be easily calculated from \autoref{eqn:T01} and
\autoref{eqn:T06} as show in \autoref{eqn:T16}. It will be used together with \autoref{eqn:T01} and related by \autoref{eqn:Td16} in order to isolate the values of $\theta_4$ and $\theta_6$.

\begin{equation}
{}^1_6\mathcal{T} = {}^1_0\mathcal{T}\ {}^0_6\mathcal{T}
\label{eqn:T16}\end{equation}
\begin{equation}
{}^1_6\mathcal{T} = {}^1_0\mathcal{T}\ \mathcal{T}_d
\label{eqn:Td16}\end{equation}

Components $21$ and $22$ of \autoref{eqn:Td16} are extracted into
\autoref{eqn:eqs4II1}. The singularity $s_5=0$ implies that $c_5=\pm 1$. Applying this relation into \autoref{eqn:eqs4II1} and grouping the left sides of the equations, one can obtain \autoref{eqn:eqs4II2}.

\begin{equation}
\begin{cases}
{}^1_6\mathcal{T}_{21} \\
{}^1_6\mathcal{T}_{22}
\end{cases}
\label{eqn:T1621and22}\end{equation}
\begin{equation}
\begin{cases}
-c_5 c_6 s_4-c_4 s_6=c_1 r_{21}-s_1 r_{11}\\
c_5 s_4 s_6-c_4 c_6=c_1 r_{22}-s_1 r_{12}
\end{cases}
\label{eqn:eqs4II1}\end{equation}
\begin{equation}
\begin{cases}
-\sin \left(\theta _6\pm \theta _4\right)=c_1 r_{21}-s_1 r_{11}\\
-\cos \left(\theta _6\pm \theta _4\right)=c_1 r_{22}-s_1 r_{12}
\end{cases}
\label{eqn:eqs4II2}\end{equation}

Combining both equations of \autoref{eqn:eqs4II2} by dividing their sides one can obtain the conditional system of equations \autoref{eqn:eq4II3}. Which can be solved into \autoref{eqn:solAxis64}.

\begin{equation}
\begin{cases}
 \tan \left(\theta _6\pm \theta _4\right)=\frac{c_1 r_{21}-s_1 r_{11}}{c_1 r_{22}-s_1 r_{12}} & -\cos \left(\theta _6\pm \theta _4\right)\neq 0 \\
 -\sin \left(\theta _6\pm \theta _4\right)=c_1 r_{21}-s_1 r_{11} & -\cos \left(\theta _6\pm \theta _4\right) = 0
\end{cases}
\label{eqn:eq4II3}\end{equation}
\begin{equation}
\theta _6\pm \theta _4=\arctan\!2\left(c_1 r_{22}-s_1 r_{12},c_1 r_{21}-s_1 r_{11}\right)
\label{eqn:solAxis64}\end{equation}

By fixing joint $4$ into its previous position and moving only joint $6$, one
can achieve a solution for both $\theta_4$ and $\theta_6$ described by
\autoref{eqn:solAxis4II} and \autoref{eqn:solAxis6II} respectively.

In this singular scenario, $\theta_4$ and $\theta_6$ provide the same effect on the end effector, therefore it is reasonable to fix one and move the other. Since joints earlier in the kinematic chain tend to be bigger and consume more energy, fixing joint 4 is desired instead of fixing joint 6.

\begin{equation}
\theta _4=\theta _{4 \text{previous}}
\label{eqn:solAxis4II}\end{equation}
\begin{equation}
\theta _6=\arctan\!2\left(c_1 r_{22}-s_1 r_{12},c_1 r_{21}-s_1 r_{11}\right)
\label{eqn:solAxis6II}\end{equation}

Provided the singular solutions of $\theta_4$ and $\theta_6$, these two joints
have now a complete solution.

\subsection{Implementation}

The solution presented has been implemented in Matlab to test the results of the direct and inverse kinematic problem solutions. The source code can be found
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/lib/kinematics/invKinUnique.m}{here}.

Additionally, the following table shows the results of the tests performed for
four possible configurations. It can be observed how the IKP solution matches
the input closely. Said test suite can be found in
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/tests/test_kinematics.m}{test\_kinematics.m}
and executed using:

\begin{lstlisting}[language=matlab,label=test-kinematics]
    run_tests 'kinematics'
\end{lstlisting}

\begin{table}[H]
\centering
\begin{tabular}{|l|l|l|l|l|l|l|l|}
\hline
                        &       & $\theta_1$ & $\theta_2$ & $\theta_3$ & $\theta_4$ & $\theta_5$ & $\theta_6$ \\ \hline
\multirow{2}{*}{Test 1} & Input & 1.1230     & 1.6194     & 1.5276     & -0.6772    & 0.9769     & -2.0660    \\ \cline{2-8} 
                        & IKP   & 1.1230     & 1.6194     & 1.5276     & -0.6772    & 0.9769     & -2.0660    \\ \hline
\multirow{2}{*}{Test 2} & Input & 1.2946     & -2.9416    & -1.4016    & -2.8515    & -2.5313    & 2.0323     \\ \cline{2-8} 
                        & IKP   & 1.2946     & -2.9416    & -1.4016    & -2.8515    & -2.5313    & 2.0323     \\ \hline
\multirow{2}{*}{Test 3} & Input & 1.2241     & -1.1492    & 2.8288     & -2.9252    & -0.3849    & -0.7442    \\ \cline{2-8} 
                        & IKP   & 1.2241     & -1.1492    & 2.8288     & -2.9252    & -0.3849    & -0.7442    \\ \hline
\multirow{2}{*}{Test 4} & Input & -2.3939    & -0.0103    & 2.8887     & -1.0029    & 0.5358     & -1.7353    \\ \cline{2-8} 
                        & IKP   & -2.3939    & -0.0103    & 2.8887     & -1.0029    & 0.5358     & -1.7353    \\ \hline
\end{tabular}
\caption{Tests performed for the IKP on MATLAB script}
\label{table:tests}
\end{table}

\subsection{Configurations}

The solutions for $\theta_1$, $\theta_2$ and $\theta_5$ are double valued,
which means that each position and orientation in the workspace has at least
$8$ possible solutions mathematically, infinite solutions in the singular
cases. In reality the manipulator has extra limitations that reduce the number
of solutions. Some of these limitations are joints motion range, collisions
with its own body and collisions with external bodies.

The mathematically non singular possible solutions have been classified using
three parameters. The value of $\theta_1$ with respect to the End Effector
position in the $XY$ plane will define whether the manipulator shoulder is in
$back$ position or not. The value of $\theta_2$ with respect to the End
Effector position will define whether the manipulator elbow is in $down$
position or not (The value of $back$ affects the value of $down$ as well).
Finally, the sign of $\theta_5$ defines whether the wrist is in $flip$ position
or not. Function
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/lib/kinematics/getConfig.m}{getConfig.m}
implements the relation between the configuration parameters, the joint angle
values and the End Effector position. The preferred configuration is not
$back$, not $down$ and not $flip$.

One can experiment with the different configurations using the test function
\href{https://gitlab.com/jemaro/wut/group-project-a/-/blob/master/tests/test_show_config.m}{test\_show\_config.m}
which will lead to illustrations like \autoref{fig:configurations}:

\begin{lstlisting}[language=matlab,label=test-kinematics]
    run_tests 'show_config'
\end{lstlisting}

Additionally, the user interface described in \autoref{sec:visualization} shows
the configuration in which the manipulator is positioned, allowing to modify it
as well. However, the physical limitations mentioned before are taken into account
in the the user interface, therefore all eight solutions are not possible at
all points within the robot workspace.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{\subdir/configurations.jpg}
    \caption{Manipulator configurations}
    \label{fig:configurations}
\end{figure}
