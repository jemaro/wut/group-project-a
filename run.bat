@REM @ECHO OFF
SET Q1=%1
SET Q2=%2
SET Q3=%3
SET Q4=%4
SET Q5=%5
SET Q6=%6

SET pwd=%~dp0
matlab -nosplash -nodesktop -r "cd %pwd%, run_visualization(%Q1%,%Q2%,%Q3%,%Q4%,%Q5%,%Q6%)"