function varargout = visualization(varargin)
    %% VISUALIZATION MATLAB code for visualization.fig
    %      VISUALIZATION, by itself, creates a new VISUALIZATION or raises the existing
    %      singleton*.
    %
    %      H = VISUALIZATION returns the handle to a new VISUALIZATION or the handle to
    %      the existing singleton*.
    %
    %      VISUALIZATION('CALLBACK',hObject,eventData,handles,...) calls the local
    %      function named CALLBACK in VISUALIZATION.M with the given input arguments.
    %
    %      VISUALIZATION('Property','Value',...) creates a new VISUALIZATION or raises the
    %      existing singleton*.  Starting from the left, property value pairs are
    %      applied to the GUI before visualization_OpeningFcn gets called.  An
    %      unrecognized property name or invalid value makes property application
    %      stop.  All inputs are passed to visualization_OpeningFcn via varargin.
    %
    %      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
    %      instance to run (singleton)".
    %
    % See also: GUIDE, GUIDATA, GUIHANDLES
    % Edit the above text to modify the response to help visualization
    %  Last Modified by GUIDE v2.5 12-May-2021 23:13:45

    % Begin initialization code - DO NOT EDIT
    gui_Singleton = 1;
    gui_State = struct('gui_Name', mfilename, ...
        'gui_Singleton', gui_Singleton, ...
        'gui_OpeningFcn', @visualization_OpeningFcn, ...
        'gui_OutputFcn', @visualization_OutputFcn, ...
        'gui_LayoutFcn', [], ...
        'gui_Callback', []);

    if nargin && ischar(varargin{1})
        gui_State.gui_Callback = str2func(varargin{1});
    end

    if nargout
        [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
    else
        gui_mainfcn(gui_State, varargin{:});
    end

    % End initialization code - DO NOT EDIT
end

% --- Executes just before visualization is made visible.
function visualization_OpeningFcn(hObject, ~, handles, varargin)
    % This function has no output args, see OutputFcn.
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)
    % varargin   command line arguments to visualization (see VARARGIN)

    % Choose default command line output for visualization
    handles.output = hObject;

    % Initialize Robot class
    handles.robot = Robot('simulate', false);

    % Build home position at the start
    if isempty(varargin)
        handles.joint_home = handles.robot.joint_home;
    elseif length(varargin) == handles.robot.num_links % Override default
        joint_home = zeros(1, handles.robot.num_links);

        for k = 1:handles.robot.num_links
            joint_home(k) = deg2rad(varargin{k});
        end

        handles.joint_home = joint_home;
    else
        error("Wrong number of arguments. %d are needed, one per link.", ...
            handles.robot.num_links, handles.robot.num_links)
    end

    % Create visualization axes
    axes(handles.visualization_axes)
    handles.robot.joint_pos = handles.joint_home;
    handles.robot.plot();
    % Initialize plot handlers
    handles.targets = [];
    handles.target_index = 1;
    handles.home = [];
    % TODO deprecate
    handles.pick = [];
    handles.place = [];
    handles.pointMode = 'target';

    % Initialize pick and place trajectory variables
    handles.trajectory_qq = [];
    handles.trajectory_changeidx = [];

    % Initialize joints plot
    handles.qq = [];
    handles = update_previous_joint_positions(hObject, false, handles, ...
        [handles.robot.joint_home; handles.robot.joint_pos]);

    %% Initialize control listeners and limits

    % Prepare joint limits removing NaN
    qlimits = handles.robot.qlimits;
    qlimits(1, :) = max(qlimits(1, :), -pi);
    qlimits(2, :) = min(qlimits(2, :), pi);

    % Joint controls
    for k = 1:handles.robot.num_links
        hSlider = handles.(sprintf('q%d_slider', k));
        % Set limits based on the robot parameters
        set(hSlider, 'min', rad2deg(qlimits(1, k)));
        set(hSlider, 'max', rad2deg(qlimits(2, k)));
        % Add listener to slider to continuously update the plot
        if k == 1 && isfield(handles, 'qslider_continuous_update')
            continue; % Avoid adding duplicate listeners
        end

        fcn = @(src, event) ...
            q_control_Callback(hSlider, 0, guidata(event.AffectedObject));
        l = addlistener(hSlider, 'Value', 'PostSet', fcn);
        handles.qslider_continuous_update(k) = l;
    end

    % End Effector controls
    for k = 1:6
        hSlider = handles.(sprintf('EE%d_slider', k));
        % Set limits based on the robot parameters
        if k < 4
            set(hSlider, 'min', handles.robot.task_bb(1, k));
            set(hSlider, 'max', handles.robot.task_bb(2, k));
        end

        % Add listener to slider to continuously update the plot
        if k == 1 && isfield(handles, 'EEslider_continuous_update')
            continue; % Avoid adding duplicate listeners
        end

        fcn = @(src, event) ...
            EE_control_Callback(hSlider, 0, guidata(event.AffectedObject));
        l = addlistener(hSlider, 'Value', 'PostSet', fcn);
        handles.EEslider_continuous_update(k) = l;
    end

    % Create a timer that will be used for delayed refresh of the controls
    handles.refresh_controls_timer = timer('Name', 'RefreshControls', ...
        'Tag', 'RefreshControls', 'ExecutionMode', 'singleShot', ...
        'TimerFcn', @(~, ~)refresh_controls(hObject, 0, guidata(hObject)));

    % Initialize the manual mode
    handles = mode_btn_Callback(...
        handles.manual_mode_btn, false, handles);

    % Initialize the text of the home position
    handles = set_home_btn_Callback(handles.set_home_btn, false, handles);

    % Initialize the trajectory mode
    handles = trajectory_mode_btn_Callback(...
        handles.joint_space_trajectory_btn, false, handles);

    % Initialize the simulator PID control
    setNumString(handles.KP, handles.robot.KP);
    setNumString(handles.KI, handles.robot.KI);
    setNumString(handles.KD, handles.robot.KD);

    % Update handles structure
    guidata(hObject, handles);

    % Update components based on the initial position passed
    start(handles.refresh_controls_timer);
end

% --- Outputs from this function are returned to the command line.
function varargout = visualization_OutputFcn(~, ~, handles)
    % varargout  cell array for returning output args (see VARARGOUT);
    % hObject    handle to figure
    % eventdata  reserved - to be defined in a future version of MATLAB
    % handles    structure with handles and user data (see GUIDATA)

    % Get default command line output from handles structure
    varargout{1} = handles.output;
end

% --- Executes when user attempts to close visualization.
function visualization_CloseRequestFcn(hObject, eventdata, handles)

    try
        handles.robot.finish_simulation();
    catch exception
        warning(exception.message)
    end
    try % Cleanup targets
        updateMode(hObject, eventdata, handles);
    catch exception
        warning(exception.message)
    end

    delete(hObject);
end

%% Plotters
function handles = plot_target(hObject, ~, handles, T)

    idx = handles.target_index;
    colors = {'b', 'm', 'c', 'r', 'g'};

    if length(handles.targets) < idx
        hold(handles.visualization_axes, 'on');
        handles.targets(idx) = trplot(T, ... 
            'axhandle', handles.visualization_axes, ... 
            'color', colors{mod(idx, length(colors))});
        hold(handles.visualization_axes, 'off');
    else
        handles.targets(idx) = trplot(T, 'handle', handles.targets(idx), ...
            'color', colors{mod(idx, length(colors))});
    end

    guidata(hObject, handles);
end

function handles = update_previous_joint_positions(hObject, ~, handles, qq)
    handles.qq = vertcat(handles.qq, qq);

    if size(handles.qq, 1) > 299
        handles.qq = handles.qq(end - 299:end, :);
    end

    uiqplot(handles.qq, handles.previous_joint_positions_axes)
    guidata(hObject, handles);
end

%% Robot wrappers

function [q, config] = task2joint(~, ~, handles, varargin)
    % Wraps robot.task2joint handling the errors that may happen
    try
        [q, config] = handles.robot.task2joint(varargin{:});
        handles.robot.validateLimits(q);
    catch exception
        warnException(exception);
        q = handles.robot.joint_pos;
        config = handles.robot.config;
    end

end

function EE = joint2task(~, ~, handles, varargin)
    % Wraps robot.joint2task handling the errors that may happen
    try
        EE = handles.robot.joint2task(varargin{:});
    catch exception
        warnException(exception);
        EE = handles.robot.task_EE;
    end

end

function qq = update_joint(hObject, eventdata, handles, index, value, varargin)
    % Wraps robot.update_joint, updates the controls after updating the plot
    try
        qq = handles.robot.update_joint(index, value);
    catch exception

        if ~(strcmp(exception.identifier, 'Robot:badinput'))
            rethrow(exception);
        end

        % TODO popup error or something
        qq = [];
        set(hObject, 'BackgroundColor', 'red');
        disp(exception.message);
        delay_refresh_controls(hObject, eventdata, handles, 1);
        return
    end

    handles = update_previous_joint_positions(hObject, eventdata, handles, qq);
    refresh_controls(hObject, eventdata, handles);
end

function qq = update_EE(hObject, eventdata, handles, EE, varargin)
    % Wraps robot.update_EE, handles the errors that may happen
    try
        qq = handles.robot.update_EE(EE);
    catch exception

        if ~(strcmp(exception.identifier, 'Robot:badinput'))
            rethrow(exception);
        end

        % TODO popup error or something
        qq = [];
        set(hObject, 'BackgroundColor', 'red');
        disp(exception.message);
        delay_refresh_controls(hObject, eventdata, handles, 1);
        return
    end

    handles = update_previous_joint_positions(hObject, eventdata, handles, qq);
    refresh_controls(hObject, eventdata, handles);
end

function qq = robot_move(hObject, eventdata, handles, method, s, varargin)
    % Wraps robot.move_joints and robot.move_EE disabling the controls while
    % the blocking code is executing
    % method: 'joints' | 'EE'
    % s: 1x6 either joint angle values or End Effector position and orientation
    refresh_controls(hObject, eventdata, handles, 'off');

    try
        qq = handles.robot.(sprintf('move_%s', method))(s, varargin{:});
    catch exception
        warnException(exception);
        qq = [];
    end

    handles = update_previous_joint_positions(hObject, eventdata, handles, qq);
    refresh_controls(hObject, eventdata, handles);
end

function qq = move_joints(hObject, eventdata, handles, q, varargin)
    qq = robot_move(hObject, eventdata, handles, 'joints', q, varargin{:});
end

function qq = move_EE(hObject, eventdata, handles, EE, varargin)
    qq = robot_move(hObject, eventdata, handles, 'EE', EE, varargin{:});
end

function [qq, configf, changeidx] = approachAndRetractTrajectory(~, ~, handles, EE, varargin)
    [qq, configf, changeidx] = handles.robot.approachAndRetractTrajectory(...
        EE, varargin{:});
end

function [qq, changeidx, handles] = pick_and_place( ... 
    hObject, eventdata, handles, EE)

    try

        if isempty(handles.trajectory_qq) % Pick has been selected
            % Compute place
            [qq, ~, changeidx] = ... 
                handles.robot.approachAndRetractTrajectory(EE);
            changeidx(3) = length(qq);
            % Store trajectory
            handles.trajectory_qq = qq;
            handles.trajectory_changeidx = changeidx;
            % Allow to select the next target
            set(hObject, 'String', 'Select Place and perform');
            handles.target_index = 2;
        else % Place has been selected
            % Get stored trajectory
            changeidx = handles.trajectory_changeidx;
            qq = handles.trajectory_qq;
            % Compute place
            [qq_place, ~, tempidx] = ... 
                handles.robot.approachAndRetractTrajectory(EE, qq(end, :));
            changeidx = [changeidx changeidx(end) + tempidx];
            qq = vertcat(qq, qq_place);
            % Perform place
            refresh_controls(hObject, eventdata, handles, 'off');
            handles.robot.do_pick_and_place(...
                qq, changeidx(2), changeidx(5));
            while handles.pick_and_place_loop.Value
                EEpick = handles.robot.joint2task(qq(changeidx(2), :));
                EEplace = handles.robot.joint2task(qq(changeidx(5), :));
                handles.robot.pick_and_place(EEpick, EEplace);
            end
            % Plot results
            handles = update_previous_joint_positions(... 
                hObject, eventdata, handles, qq);
            % Cleanup
            handles = updateMode(hObject, eventdata, handles);
        end

    catch exception
        warnException(exception);
    end

    refresh_controls(hObject, eventdata, handles);
end

%% Utility functions

function warnException(exception)
    if ~(strcmp(exception.identifier, 'Robot:badinput'))
        rethrow(exception);
    end

    warndlg(exception.message, exception.identifier, 'modal');

end

function num = setNumString(hObject, num)
    % Validates and sets a numerical string into an edit text control
    % hObject: edit text handle
    % num: string containing the number or the number itself
    if ~isnumeric(num)
        num = str2double(num);
    end

    if isnan(num)
        warndlg('Input must be a scalar');
        num = 0;
    end

    % Round numbers to decimals
    num = round(num, 4, 'significant');
    set(hObject, 'String', num2str(num));
end

function s = getSliderValues(handles, is_joint)
    % Gets the slider values. The values of angles are converted to radians in
    % order to make its usage easier.
    % is_joint: Whether to get joint angles or End Effector position and
    % orientation

    if is_joint
        control = 'q';
    else
        control = 'EE';
    end

    s = zeros(1, 6);

    for k = 1:6
        s(k) = get(handles.(sprintf('%s%d_slider', control, k)), 'Value');
    end

    if is_joint
        s = deg2rad(s);
    else
        s(4:6) = deg2rad(s(4:6));
    end

end

function handles = updateMode(hObject, eventdata, handles)
    % Enable the move button and trajectory modes while in auto mode
    handles.is_manual = handles.manual_mode_btn.Value;
    handles.is_auto = handles.auto_mode_btn.Value;
    handles.is_pick_and_place = handles.pick_and_place_mode_btn.Value;

    if handles.is_manual
        set(handles.move_btn, 'String', 'Move');
    elseif handles.is_auto
        set(handles.move_btn, 'String', 'Move');
    elseif handles.is_pick_and_place
        set(handles.move_btn, 'String', 'Select Pick');
    else
        error('Mode not implemented: %d', m);
    end

    % Delete target plots if exist
    try
        for target = handles.targets
            delete(target);
        end
    catch exception
        warning(exception.message)
    end

    handles.targets = [];
    handles.target_index = 1;

    % Clear the stored trajectory
    handles.trajectory_qq = [];

    % Refresh controls
    handles = refresh_controls(hObject, eventdata, handles);
    guidata(hObject, handles);
end

function handles = refresh_q_controls(hObject, ~, handles, enable)
    % Refresh joint controls
    % enable: 'on' | 'off' | 'inactive'

    q = handles.robot.joint_pos;

    for k = 1:length(q)
        % Pause the continuous update to avoid infinite loops
        continuous_update = handles.qslider_continuous_update(k).Enabled;
        handles.qslider_continuous_update(k).Enabled = 0;
        val = rad2deg(q(k));
        hText = handles.(sprintf('q%d_text', k));
        setNumString(hText, val);
        set(hText, 'BackgroundColor', 'white');
        set(hText, 'Enable', enable);
        hSlider = handles.(sprintf('q%d_slider', k));
        set(hSlider, 'Value', val);
        set(hSlider, 'BackgroundColor', [.9 .9 .9]);
        set(hSlider, 'Enable', enable);
        % Resume the continuous update
        handles.qslider_continuous_update(k).Enabled = continuous_update;
    end

    guidata(hObject, handles);
end

function handles = refresh_EE_controls(hObject, ~, handles, enable)
    % Refresh End Effector controls
    % enable: 'on' | 'off' | 'inactive'

    EE = handles.robot.task_EE;

    for k = 1:length(EE)
        % Pause the continuous update to avoid infinite loops
        continuous_update = handles.EEslider_continuous_update(k).Enabled;
        handles.EEslider_continuous_update(k).Enabled = 0;
        val = EE(k);

        if k > 3 % Orientation in degrees
            val = rad2deg(val);
        end

        hText = handles.(sprintf('EE%d_text', k));
        setNumString(hText, val);
        set(hText, 'BackgroundColor', 'white');
        set(hText, 'Enable', enable);
        hSlider = handles.(sprintf('EE%d_slider', k));
        set(hSlider, 'Value', val);
        set(hSlider, 'BackgroundColor', [.9 .9 .9]);
        set(hSlider, 'Enable', enable);
        % Resume the continuous update
        handles.EEslider_continuous_update(k).Enabled = continuous_update;
    end

    guidata(hObject, handles);
end

function handles = refresh_configuration_controls(hObject, ~, handles, enable)
    % Refresh configuration controls
    % enable: 'on' | 'off' | 'inactive'
    for k = 1:3
        h = handles.(sprintf('config%d', k));
        set(h, 'Value', handles.robot.config(k));
        set(h, 'Enable', enable);
    end

    set(handles.gripper_closed, 'Value', handles.robot.gripper_closed);
    if handles.robot.gripper_closed
        set(handles.gripper_closed, 'String', 'Gripper Closed');
    else
        set(handles.gripper_closed, 'String', 'Gripper Open');
    end
    set(handles.gripper_closed, 'Enable', enable);

    guidata(hObject, handles);
end

function handles = refresh_trajectory_controls(hObject, ~, handles, enable)
    % Refresh trajectory controls
    % enable: 'on' | 'off' | 'inactive'
    set(handles.joint_space_trajectory_btn, 'Enable', enable);
    set(handles.task_space_trajectory_btn, 'Enable', enable);

    guidata(hObject, handles);
end

function handles = refresh_controls(hObject, eventdata, handles, enable)
    % Refreshes all the controls
    % enable: 'on' | 'off' | 'inactive'

    if nargin < 4

        if handles.is_manual
            enable_trajectory = 'off';
            enable_configuration = 'on';
            enable_move = 'off';
        elseif handles.is_auto
            enable_trajectory = 'on';
            enable_configuration = 'on';
            enable_move = 'on';
        else
            enable_trajectory = 'off';
            enable_configuration = 'off';
            enable_move = 'on';
        end

        enable = 'on';
    else
        enable_trajectory = enable;
        enable_configuration = enable;
        enable_move = enable;
    end

    % Update mode controls
    handles = refresh_q_controls(hObject, eventdata, handles, enable);
    handles = refresh_EE_controls(hObject, eventdata, handles, enable);
    handles = refresh_configuration_controls(...
        hObject, eventdata, handles, enable_configuration);
    handles = refresh_trajectory_controls(...
        hObject, eventdata, handles, enable_trajectory);
    if handles.is_pick_and_place
        set(handles.pick_and_place_loop, 'Enable', 'on');
    else
        set(handles.pick_and_place_loop, 'Enable', 'off');
    end
    set(handles.move_btn, 'Enable', enable_move);

    % Enable/Disable buttons
    set(handles.move_home_btn, 'Enable', enable);
    set(handles.set_home_btn, 'Enable', enable);
    set(handles.view_home_btn, 'Enable', enable);
    set(handles.manual_mode_btn, 'Enable', enable);
    set(handles.auto_mode_btn, 'Enable', enable);
    set(handles.pick_and_place_mode_btn, 'Enable', enable);
    if length(enable) > 2 % 'off' or 'inactive'
        set(handles.previous_joint_positions_axes, 'Color', [0.8 0.8 0.8]);
    end

    guidata(hObject, handles);
end

function delay_refresh_controls(~, ~, handles, delay)
    stop(handles.refresh_controls_timer)
    delay = delay / (60^2 * 24);
    startat(handles.refresh_controls_timer, now + delay)
end

function [idx, val, is_slider] = control_Callback(... 
    hObject, ~, handles, control, tag2index)
    % Arguments
    % control: String that specifies what is being controled. 'q' | 'EE'
    % tag2index: Position of the index in the Tag attribute. Joint angles
    % controls are named like 'q2_text' and 'q5_slider'. Therefore its
    % tag2index is 2, because the index is the second character in the tag
    % Output
    % idx: index of the controled variable
    % val: value of the controled variable

    style = get(hObject, 'Style');
    tag = get(hObject, 'Tag');
    idx = str2double(tag(tag2index));

    if style(1) == 's' % slider
        val = get(hObject, 'Value');
        hText = handles.(sprintf('%s%d_text', control, idx));
        setNumString(hText, val);
        is_slider = true;
    else % text
        val = setNumString(hObject, get(hObject, 'String'));
        hSlider = handles.(sprintf('%s%d_slider', control, idx));
        set(hSlider, 'Value', val);
        is_slider = false;
    end

end

%% Joint Angles Controls

function q_slider_CreateFcn(hObject, ~, ~)
    % hObject    handle to joint angle control slider
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
        set(hObject, 'BackgroundColor', [.9 .9 .9]);
    end

end

function q_text_CreateFcn(hObject, ~, ~)
    % hObject    handle to slider (see GCBO)
    % Hint: edit controls usually have a white background on Windows.
    if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
        set(hObject, 'BackgroundColor', 'white');
    end

end

function q_control_Callback(hObject, eventdata, handles)
    % hObject    handle to q1_text (see GCBO)
    % handles    structure with handles and user data (see GUIDATA)
    [jointidx, val, is_slider] = control_Callback( ... 
        hObject, eventdata, handles, 'q', 2);

    if is_slider % The text updates slider, we don't want duplicated updates
        if handles.is_manual
            update_joint(hObject, eventdata, handles, jointidx, deg2rad(val));
        else
            q = getSliderValues(handles, true);
            set(hObject, 'BackgroundColor', 'yellow');
            T = handles.robot.dirKinEE(q);
            handles = plot_target(hObject, eventdata, handles, T);
            handles = refresh_EE_controls(hObject, eventdata, handles, 'on');
            guidata(hObject, handles);
        end
    end
end

%% End Effector Controls
function EE_slider_CreateFcn(hObject, ~, ~)
    % hObject    handle to joint angle control slider
    % Hint: slider controls usually have a light gray background.
    if isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
        set(hObject, 'BackgroundColor', [.9 .9 .9]);
    end

end

function EE_text_CreateFcn(hObject, ~, ~)
    % hObject    handle to the EE_text (see GCBO)
    % Hint: edit controls usually have a white background on Windows.
    if ispc && isequal(get(hObject, 'BackgroundColor'), get(0, 'defaultUicontrolBackgroundColor'))
        set(hObject, 'BackgroundColor', 'white');
    end

end

function EE_control_Callback(hObject, eventdata, handles)
    % hObject    handle to the EE control
    [EEidx, val, is_slider] = control_Callback( ... 
        hObject, eventdata, handles, 'EE', 3);

    if is_slider % The text updates slider, we don't want duplicated updates
        if handles.is_manual
            EE = handles.robot.task_EE;

            if EEidx > 3 % The orientation is in degrees in the control
                EE(EEidx) = deg2rad(val);
            else
                EE(EEidx) = val;
            end

            update_EE(hObject, eventdata, handles, EE);
        else
            EE = getSliderValues(handles, false);
            set(hObject, 'BackgroundColor', 'yellow');
            T = task2matrixT(EE);
            handles = plot_target(hObject, eventdata, handles, T);
            handles = refresh_q_controls(hObject, eventdata, handles, 'on');
            guidata(hObject, handles);
        end
    end
end

%% Configuration controls

function config_checkbox_Callback(hObject, eventdata, handles)
    % hObject    handle to the configuration checkbox
    tag = get(hObject, 'Tag');
    configidx = str2double(tag(7)); % config1 <- the number is the 7th character
    config = handles.robot.config;
    config(configidx) = get(hObject, 'Value');
    EE = handles.robot.task_EE;
    q = handles.robot.task2joint(EE, handles.robot.joint_pos, config);
    move_joints(hObject, eventdata, handles, q);
end

%% Robot Control and Mode

function handles = mode_btn_Callback(hObject, eventdata, handles)
    % TODO make menu work again
    % set(handles.manual_mode_menuItem, 'Checked', is_manual);
    handles = updateMode(hObject, eventdata, handles);
    guidata(hObject, handles);
end

function move_btn_Callback(hObject, eventdata, handles)
    % Check if the joint controls are different than the robot position and get
    % both the joint space and task space variables
    q_current = handles.robot.joint_pos;
    q = getSliderValues(handles, true);

    if ismembertol(q_current, q, 2e-4, 'ByRows', true)
        EE = getSliderValues(handles, false);
        q = task2joint(hObject, eventdata, handles, EE);
    else
        EE = joint2task(hObject, eventdata, handles, q);
    
    end

    % Move in auto mode
    if handles.is_auto

        if handles.is_joint_trajectory
            move_joints(hObject, eventdata, handles, q);
        else
            move_EE(hObject, eventdata, handles, EE);
        end

        % Pick and place flow
    elseif handles.is_pick_and_place
        [~, ~, handles] = pick_and_place(hObject, eventdata, handles, EE);
    else
        error('Visualization:programming_error', ...
            'Move button pressed is_manual = %d', handles.is_manual);
    end

    guidata(hObject, handles);
end

function gripper_closed_Callback(hObject, eventdata, handles)
    handles.robot.update_gripper(hObject.Value);
    refresh_configuration_controls(hObject, eventdata, handles, 'on');
end


function handles = trajectory_mode_btn_Callback(hObject, ~, handles)
    handles.is_joint_trajectory = handles.joint_space_trajectory_btn.Value;
    guidata(hObject, handles);
end

%% Home control
function move_home_btn_Callback(hObject, eventdata, handles)
    move_joints(hObject, eventdata, handles, handles.robot.joint_home);
end

function view_home_btn_Callback(hObject, ~, handles)

    if hObject.Value
        hold(handles.visualization_axes, 'on')
        T = handles.robot.dirKinEE(handles.robot.joint_home);
        handles.home = trplot(T, 'axhandle', handles.visualization_axes, ...
            'color', 'k');
        hold(handles.visualization_axes, 'off')
    else
        delete(handles.home);
        handles.home = [];
    end

    guidata(hObject, handles)
end

function handles = set_home_btn_Callback(~, ~, handles)
    % Set home to the current position
    handles.robot.joint_home = handles.robot.joint_pos;

    % Update text
    EE = handles.robot.task_EE;

    for k = 1:6
        setNumString(handles.(sprintf('homeEE%d_text', k)), EE(k));
    end

    % Update plot
    if ~isempty(handles.home)
        T = handles.robot.dirKinEE(handles.robot.joint_home);
        handles.home = trplot(T, 'handle', handles.home, 'color', 'k');
    end

end

% %% Menu a third mode makes this to not work

% % Control
% function control_menu_Callback(hObject, eventdata, handles)
%     % hObject    handle to control_menu (see GCBO)
%     % eventdata  reserved - to be defined in a future version of MATLAB
%     % handles    structure with handles and user data (see GUIDATA)
% end

% function move_home_menuItem_Callback(hObject, eventdata, handles)
%     % hObject    handle to move_home_btn (see GCBO)
%     % eventdata  reserved - to be defined in a future version of MATLAB
%     % handles    structure with handles and user data (see GUIDATA)
% end

% function manual_mode_menuItem_Callback(hObject, eventdata, handles)
%     % hObject    handle to manual_mode_menuItem (see GCBO)
%     % eventdata  reserved - to be defined in a future version of MATLAB
%     % handles    structure with handles and user data (see GUIDATA)
%     is_manual = ~get(hObject, 'Checked');
%     set(hObject, 'Checked', is_manual);
%     set(handles.manual_mode_checkbox, 'Value', is_manual);
%     handles = updateManualMode(hObject, eventdata, handles, is_manual);
%     guidata(hObject, handles);
% end

%% Simulator

function connect_simulator_Callback(hObject, ~, handles)

    if hObject.Value

        try
            handles.robot.start_simulation();
        catch exception
            hObject.Value = false;
            errordlg( ... 
                sprintf('%s: %s', exception.identifier, exception.message), ... 
                'Failed', 'modal');
        end

    else
        handles.robot.finish_simulation();
    end

    guidata(hObject, handles);
end

function handles = simulator_PID_Callback(hObject, ~, handles)
    value = setNumString(hObject, hObject.String);
    handles.robot.(hObject.Tag) = value;
    guidata(hObject, handles);
end

%% Visualization

function clear_plot_trail_Callback(~, ~, handles)
    handles.robot.clear_plot_trail()
end
