function [vq, gripper_close] = control(robot, q, gripper_closed, t)
    % q: Current joint angle values in radians
    % gripper_closed: Current position of the gripper being 0 completely open
    % and 1 completely closed
    % t: Simulation time in ms

    % Initialize output
    gripper_close = robot.gripper_closed;

    % The persistent variables will act as our robot memory
    persistent q_1 q_error_1 PID_1 vq_1 gripper_closed_1

    % Initialize memory
    if isempty(q_1)
        q_1 = q;
        vq_1 = zeros(1, 6);
        q_error_1 = zeros(1, 6);
        PID_1 = zeros(1, 6);
        gripper_closed_1 = -gripper_closed;
    end

    switch robot.state
        case 0 % IDLE
            q_error = angdiff(robot.joint_pos, q);
            v_error = angdiff(q_1, q) / robot.timestep;
        case 1 % Trajectory
            % Consume trajectory
            q_traj = robot.sim_trajectory(1, :);
            robot.sim_trajectory = robot.sim_trajectory(2:end, :);
            q_error = angdiff(q_traj, q);
            % Termination condition
            if isempty(robot.sim_trajectory)
                robot.state = 0; % Back to IDLE
                v_error = angdiff(q_1, q) / robot.timestep;
            else
            % Feed forward
                q_future = robot.sim_trajectory(1, :);
                v_error = (angdiff(q_1, q) - angdiff(q_traj, q_future))  ... 
                    / robot.timestep;
            end
        case 2 % Gripper
            % Stand still until gripper is in the correct position
            q_error = angdiff(robot.joint_pos, q);
            v_error = angdiff(q_1, q) / robot.timestep;
            % Termination condition
            if gripper_closed*(2*gripper_close - 1) > gripper_close
                robot.state = 0;
            else
                % If gripper hasn't changed for a while stop it too
                dgc = abs(gripper_closed - gripper_closed_1);
                if dgc < 1e-4 % Gripper change tolerance
                    robot.state = 0;
                    gripper_closed_1 = -gripper_closed;
                else
                    gripper_closed_1 = gripper_closed;
                end
            end
        otherwise
            error('Control:programming_error', 'Wrong state %d', robot.state)
    end

    % PID
    P = robot.KP .* q_error;
    D = robot.KD .* v_error;
    I = robot.KI .* (q_error + q_error_1 - (PID_1 - vq_1) / robot.KP); % With anti-windup
    PID = P + I + D;
    vq = max(min(PID, robot.vqmax), -robot.vqmax);

    % Save new memory
    q_1 = q;
    q_error_1 = q_error;
    PID_1 = PID;
    vq_1 = vq;
end
