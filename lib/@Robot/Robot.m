classdef Robot < handle

    properties
        % Robot movement parameters
        joint_pos = [];
        task_EE;
        config = [];
        gripper_closed = false;
        % Trajectory prameters
        trajectory_method = 'cubic';
        timestep = 0.05; % [s]
        joint_vmax = 0.8; % [rad/s]
        task_vmax_lin = 0.2; % [task space units/s]
        task_vmax_ang = 1; % [rad/s]
        approach_zoffset = 0.1;
        % Simulation control parameters
        state = 0;
        vqmax = 2 * pi;
        KP = 17;
        KI = 1;
        KD = 0.5;
        % Set on initialization;
        name = '';
        num_links = 0;
        dh = [];
        qlimits = []; % 2x6 array: [q1_min ... q6_min; q1_max ... q6_max]
        joint_home = [];
        task_bb = []; % Bounding box: [xmin, ymin, zmin; xmax, ymax, zmax]
        % Additional parameters used on the visualization
        plot_trail_limit = 50;
    end

    properties (Access = private)
        robot_viz = [];
        plot_bb = []; % Bounding box: [xmin, ymin, zmin; xmax, ymax, zmax]
        plot_handles;
        % Simulation parameters
        simulation; % Handle to the simulation
        sim_trajectory = []; % This array will be consumed by the simulator
    end

    methods

        function obj = Robot(varargin)
            % Input parser
            p = inputParser;
            validNum = @(x) isnumeric(x) && (x > 0);
            % ? is it worth a vector of default values for easy modifications ?
            addOptional(p, 'd1', 0.125, validNum);
            addOptional(p, 'a2', 0.30, validNum);
            addOptional(p, 'a3', 0.15, validNum);
            addOptional(p, 'd4', 0.25, validNum);
            addOptional(p, 'd6', 0.175, validNum);

            % Home position
            validVector = @(x) all(isnumeric(x)) && all(x >= -pi) ...
                && all(x <= pi);
            defaultHome = [0 -0.1 0 0 0.1 0];
            addOptional(p, 'home', defaultHome, validVector);

            % Joint offsets
            defaultOffsets = [0, -pi / 2, 0, 0, 0, 0];
            addOptional(p, 'jointOffsets', defaultOffsets, validVector);

            % Joint limits
            validLimit = @(x) validVector(x(1, :)) && validVector(x(2, :)) ...
                && sum(x(1, :) < x(2, :), 2) == length(x);
            % TODO adjust positive limit of joint 3, maybe using a3 and d4
            % limits: 2x6 array of joint limit values
            % [q1min, q2min, q3min, q4min, q5min, q6min;
            %  q1max, q2max, q3max, q4max, q5max, q6max]
            defaultLimits = [NaN, -pi / 2, -3 * pi / 4, NaN, -pi / 2, NaN;
                        NaN, +pi / 2, +pi / 2, NaN, +pi / 2, NaN];
            addOptional(p, 'jointLimits', defaultLimits, validLimit);

            % Robot name
            addOptional(p, 'name', 'Group A', @(x) isstring(x) || ischar(x));

            % Connect to simulator
            isBoolean = @(x)(islogical(x) || isnumeric(x)) && sum(size(x)) == 2;
            addOptional(p, 'simulate', false, isBoolean);

            parse(p, varargin{:})
            params = p.Results;

            % Set Denavit-Hatenberg parameters
            % dh: [theta_offset, d, a, alpha]
            obj.dh = [
                params.jointOffsets(1) params.d1 0 0
                params.jointOffsets(2) 0 0 3 * pi / 2
                params.jointOffsets(3) 0 params.a2 0
                params.jointOffsets(4) params.d4 params.a3 3 * pi / 2
                params.jointOffsets(5) 0 0 pi / 2
                params.jointOffsets(6) params.d6 0 3 * pi / 2
                ];
            obj.num_links = length(obj.dh);
            obj.joint_home = params.home;
            obj.joint_pos = obj.joint_home;
            obj.config = obj.getConfig(obj.joint_pos);
            obj.qlimits = params.jointLimits;
            max_dist = params.a2 + sqrt(params.d4^2 + params.a3^2) + params.d6;
            % Limit workspace in order to avoid sinking in the floor
            obj.task_bb = [-max_dist, -max_dist, 0.1; ...
                            max_dist, max_dist, max_dist + params.d1];
            % Adds an offset to the bounding box for the plot
            obj.plot_bb = obj.task_bb .* 1.25;
            obj.plot_bb(1, 3) = 0; %Plot until floor

            % Initialize RVC Toolbox
            obj.name = params.name;
            obj.robot_viz = obj.init_rvctools();

            % Initialize simulator
            if params.simulate

                try
                    obj.start_simulation();
                catch exception
                    fprintf('Simulation could not be initialized: %s \n', ...
                        exception.message);
                end

            end

        end

        function serial_link = init_rvctools(obj)
            % ? Preallocate L?
            for k = 1:size(obj.dh, 1)
                % Link([theta, d, a, alpha, isprismatic, offset], options{:})
                L(k) = Link([0 obj.dh(k, 2:4) 0 obj.dh(k, 1)], 'modified');
            end

            serial_link = SerialLink(L);
            serial_link.name = obj.name;
        end

        function start_simulation(obj)
            % Wraps the Simulation constructor
            obj.simulation = Simulation(@obj.control, ...
                'timestep', obj.timestep, ...
                'start_joints', obj.joint_pos);
            obj.simulation.start();
        end

        function finish_simulation(obj)
            % Wraps the Simulation.finish method
            if ~isempty(obj.simulation)
                obj.simulation.finish()
                obj.simulation = [];
            end

        end

        function res = is_simulated(obj)
            res = ~isempty(obj.simulation);
        end

        function delete(obj)
            % Class destructor. When Robot is deleted it will stop the
            % simulation
            obj.finish_simulation()
        end

        function plot(obj, qq, varargin)
            % RVC plot wrapper. It passes the optional arguments together with
            % some defaults defined by plot_options. If no input is provided,
            % the current robot position will be plotted.
            %
            % See https://github.com/petercorke/robotics-toolbox-matlab/blob/bd7a9d75176c660f43fc799b24d838f70b02250c/%40SerialLink/plot.m
            if nargin == 1
                qq = obj.joint_pos;
            end

            if isempty(obj.plot_handles) || ~isempty(varargin)
                opt = {'delay', obj.timestep, ...
                    'trail', {'Color', [0.5 0.1 0.1], 'LineWidth', 1}, ...
                    'scale', 0.8, 'jointdiam', 1.2, 'floorlevel', 0, ...
                    'workspace', obj.plot_bb(:), 'noname', ... 
                    'linkcolor', [0.4 0.3 0.4], ...
                    'jointcolor', [0.2 0.1 0.2]};
                % Initialize plot
                obj.plot_handles = obj.robot_viz.plot(...
                    qq, opt{:}, varargin{:});
                % Initialize the robot gripper color
                obj.update_gripper(obj.gripper_closed);
            else
                obj.robot_viz.animate(qq, obj.plot_handles);
                % Limit the robot trail
                if size(obj.robot_viz.trail, 1) > obj.plot_trail_limit
                    obj.robot_viz.trail = obj.robot_viz.trail(...
                        end - obj.plot_trail_limit:end, :);
                end

            end

        end

        function clear_plot_trail(obj)
            obj.robot_viz.trail = [];
            obj.plot();
        end

        function do_trajectory(obj, qq, varargin)
            % Blocks the code until trajectory is finished
            if obj.is_simulated
                obj.sim_trajectory = qq;
                obj.state = 1; % Trajectory
                waitfor(obj, 'state'); % Wait until the state is changed
                obj.update_joints(qq(end, :));
            else
                obj.plot(qq, varargin{:});
                obj.joint_pos = qq(end, :);
            end

        end

        function EE = get.task_EE(obj)
            EE = obj.joint2task(obj.joint_pos);
        end

        function set.task_EE(~, ~)
            error("Can't set 'task_EE' directly, use 'update_EE'.")
        end

        function validateLimits(obj, q)
            % TODO improve error
            assert(all(validateLimits(q, obj.qlimits)), 'Robot:badinput', ...
                'Objective is out of joint limits.')
        end

        function validateBoundingBox(obj, EE)
            % TODO improve error
            assert(all(validateBoundingBox(EE, obj.task_bb)), ...
                'Robot:badinput', 'Objective is out of task limits.')
        end

        function T = dirKin(obj, q)
            % Gets a 1x6 vector with joint values [q1, q2, q3, q4, q5, q6] and
            % returns an array 4x4x6 of the homogeneous transformation matrices
            % of the different links
            T = dirKin(q, obj.dh);
        end

        function T = dirKinEE(obj, q)
            % Gets a Nx6 array with joint values [q1, q2, q3, q4, q5, q6] and
            % returns a 4x4 homogeneous transformation matrx for the End
            % Effector
            T = dirKinEE(q, obj.dh);
        end

        function EE = joint2task(obj, q)
            % Gets a Nx6 array with joint values [q1, q2, q3, q4, q5, q6] and
            % returns task end effector position and orientation using direct
            % kinematics.
            % EE: Nx6
            EE = joint2task(q, obj.dh);
        end

        % TODO document
        function qq = jointTrajectory(obj, q0, v0, qf, vf)
            qq = jointTrajectory(obj.trajectory_method, obj.qlimits, ...
                q0, v0, qf, vf, obj.joint_vmax, obj.timestep);
        end

        % TODO document
        function all_q = invKin(obj, EE, q_previous)

            if nargin < 3
                q_previous = obj.joint_pos;
            end

            T = task2matrixT(EE);
            all_q = invKin(T, q_previous, obj.dh);
        end

        function q = invKinUnique(obj, EE, q_previous, config)
            % TODO docu
            % Usage:
            % q = robot.invKinUnique(EE, q_previous, config)
            % q = robot.invKinUnique(EE)
            % q = robot.invKinUnique(EE, q_previous)
            % q = robot.invKinUnique(EE, config)

            if nargin < 3
                q_previous = obj.joint_pos;
                config = obj.config;
            elseif nargin == 3

                if size(q_previous, 2) == 3
                    config = q_previous;
                    q_previous = obj.joint_pos;
                else
                    config = obj.config;
                end

            end

            T = task2matrixT(EE);
            q = invKinUnique(T, q_previous, obj.dh, config);
        end

        function config = getConfig(obj, q, T)
            % TODO docu

            if nargin == 2
                T = dirKinEE(q, obj.dh);
            end

            config = getConfig(q, T, obj.dh);
        end

        function [q, config] = task2joint(obj, EE, q_previous, config)
            % Gets 6xn matrix with [x, y, z, gamma, beta, alpha] and returns
            % joint values using inverse kinematics. It should take into
            % account the last position in order to reduce the solutions of the
            % IKP to 1.
            % q = robot.task2joint(EE) % Use current position and configuration
            % q = robot.task2joint(EE, q_prev) % Get configuration from q_prev
            % q = robot.task2joint(EE, q_prev, config)
            if nargin < 3
                q_previous = obj.joint_pos;
                config = obj.config;
            elseif nargin == 3
                config = obj.getConfig(q_previous, EE);
            end

            q = task2joint(EE, q_previous, obj.dh, config);
        end

        % TODO document
        function [qq, configf, startidx] = taskTrajectory(obj, config0, ...
                q0, vEE0, EEf, vEEf)
            [qq, configf, startidx] = taskTrajectory(obj.trajectory_method, ...
                obj.qlimits, config0, q0, vEE0, EEf, vEEf, ...
                obj.task_vmax_lin, obj.task_vmax_ang, obj.timestep, obj.dh);
        end

        function q = update_joints(obj, q)
            % Changes the position of the manipulator joints without generating
            % a trajectory and updates the plot.
            % q: 1x6 vector [q1, q2, q3, q4, q5, q6]
            obj.validateLimits(q);
            EE = obj.joint2task(q);
            obj.validateBoundingBox(EE);
            obj.joint_pos = q;
            obj.config = obj.getConfig(obj.joint_pos);
            obj.plot();
        end

        function q = update_joint(obj, index, value)
            % Changes the position of one of the manipulator joints without
            % generating a trajectory and updates the plot.
            % index: integer between 1 and 6 that indicates which is the joint
            % is being changed
            % value: joint value in radians
            q = obj.joint_pos;
            q(index) = value;
            obj.update_joints(q);
        end

        function q = update_EE(obj, EE)
            % Changes the position of the manipulator joints to match a desired
            % End Effector position and orientation in the task space without
            % generating a trajectory and updates the plot.
            % EE: 1x6 vector with the desired position and orientation of the
            % End Effector [x, y, z, gamma, beta, alpha]
            obj.validateBoundingBox(EE);
            q = obj.task2joint(EE);
            obj.validateLimits(q);
            obj.update_joints(q);
        end

        function update_gripper(obj, closed)
            obj.gripper_closed = closed;
            % Update the plot color
            if closed, col = 'r'; else, col = 'b'; end

            if ~isempty(obj.plot_handles)

                for handle = obj.plot_handles
                    h = get(handle, 'UserData');
                    % FaceColor
                    for child = h.link(6).Children
                        set(child, 'FaceColor', col)
                    end

                end

            end

            if obj.is_simulated
                obj.state = 2; % Gripper
                waitfor(obj, 'state'); % Wait until the state is changed
            else
                pause(0.2);
            end

        end

        function qq = move_joints(obj, q, varargin)
            % Changes the position of the manipulator joints generating a
            % trajectory and animating the plot. It will block the code while
            % the animation is running.
            % q: 1x6 vector [q1, q2, q3, q4, q5, q6]
            % qq: Nx6 trajectory array [q1, q2, q3, q4, q5, q6; ...]

            % If initial and final positions are equivalent, there is no
            % movement
            if ismembertol(obj.joint_pos, q, 2e-4, 'ByRows', true)
                qq = [];
                return
            end

            % Initial and final joint velocities will be zero
            v = zeros(1, obj.num_links);
            qq = obj.jointTrajectory(obj.joint_pos, v, q, v);
            obj.do_trajectory(qq, varargin{:});
            obj.config = obj.getConfig(obj.joint_pos);
        end

        function [qq, configf, startidx] = move_EE(obj, EE, varargin)
            % Changes the position of the manipulator joints to match a desired
            % End Effector position and orientation in the task space
            % generating a trajectory and animating the plot. It will block the
            % code while the animation is running.
            % EE: 1x6 vector with the desired position and orientation of the
            % End Effector [x, y, z, gamma, beta, alpha]

            % Initial and final joint velocities will be zero
            vEE = zeros(1, obj.num_links);
            [qq, configf, startidx] = obj.taskTrajectory(obj.config, ...
                obj.joint_pos, vEE, EE, vEE);
            obj.do_trajectory(qq, varargin{:});
            % Update the robot position and configuration
            obj.config = configf;
        end

        % TODO documentation
        function [qq, configf, changeidx] = approachAndRetractTrajectory(...
                obj, EE, q_previous, zoffset)
            % Trajectory phases:
            % 1 Go to approach start point
            % 2 Approach to target
            % 3 Retract to the approach start point
            % EE: 1x6 target as End Effector position and orientation
            % [x, y, z, gamma, beta, alpha]
            % q_previous:
            % zoffset:
            if nargin < 3
                q_previous = obj.joint_pos;
                zoffset = obj.approach_zoffset;
            elseif nargin < 4
                zoffset = obj.approach_zoffset;
            end

            changeidx = zeros(1, 2);
            % TODO blend joint and task trajectory
            v0 = zeros(1, obj.num_links);

            % Compute second key point and validate it, we'll assume that the
            % starting point is valid already
            EEoffset = offsetEEz(EE, -zoffset);
            obj.validateBoundingBox([EE; EEoffset]);
            [qoffset, config0] = obj.task2joint(EEoffset, q_previous);
            % Compute first the task trajectory in order to check if a change
            % of configuration will be needed
            [qq_a, configf, startidx] = obj.taskTrajectory(...
                config0, qoffset, v0, EE, v0);

            if startidx > 1 % Change in configuration
                qoffset = qq_a(startidx - 1, :);
                qq_a = qq_a(startidx:end, :);
            end

            % Compute the joint trajectory until the approach start
            qq = obj.jointTrajectory(q_previous, v0, qoffset, v0);
            changeidx(1) = length(qq) + 1; % Approach start
            qq = vertcat(qq, qq_a);
            changeidx(2) = length(qq) + 1; % Approach end
            qq = [qq; flipud(qq_a)];
            % Validate the resulting trajectory
            obj.validateLimits(qq);
        end

        function [qq, changeidx] = pickAndPlaceTrajectory(obj, EEpick, EEplace)
            % changeidx: array of indices where the trajectory changes phase.
            % There are 6 phases, therefore there are 5 phase changes.
            %
            % Trajectory phases:
            % 1 Go to pick: changeidx(1) = pick approach starting point
            % 2 Approach: changeidx(2) = pick point
            % 3 Retract: changeidx(3) = pick approach starting point
            % 4 Go to place: changeidx(4) = place approach starting point
            % 5 Approach: changeidx(5) = place point
            % 6 Retract: length(qq) = place approach starting point

            % Compute pick
            [qq, ~, changeidx] = obj.approachAndRetractTrajectory(EEpick);
            changeidx(3) = length(qq);
            % Compute place
            [qq_place, ~, tempidx] = obj.approachAndRetractTrajectory(...
                EEplace, qq(end, :));
            % Combine trajectories
            changeidx = [changeidx changeidx(end) + tempidx];
            qq = vertcat(qq, qq_place);
        end

        function qq = do_pick_and_place(obj, qq, pickidx, placeidx)
            % Takes care of the gripper movement appart of following the
            % trajectory
            obj.update_gripper(false);
            obj.do_trajectory(qq(1:pickidx, :)); % Go to pick
            obj.update_gripper(true);
            obj.do_trajectory(qq(pickidx + 1:placeidx, :)); % Go to place
            obj.update_gripper(false);
            obj.do_trajectory(qq(placeidx + 1:end, :))
        end

        function [qq, changeidx] = pick_and_place(obj, EEpick, EEplace, varargin)
            % Calculate the pick and place trajectory and perform it
            [qq, changeidx] = obj.pickAndPlaceTrajectory(EEpick, EEplace);
            obj.do_pick_and_place(qq, changeidx(2), changeidx(5));
        end

    end

end
