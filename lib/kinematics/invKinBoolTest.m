function flag = invKinBoolTest(T, q_previous, dh)
    try
        invKin(T, q_previous, dh);
        flag = 1;
    % TODO catch only singularity related errors
    catch
        flag = 0;
    end