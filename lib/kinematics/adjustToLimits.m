function q = adjustToLimits(q, limits)
% This function adjusts vector q to limits max and min values
q(q>limits(1,:))=limits(1,q>limits(1,:));
q(q<limits(2,:))=limits(2,q<limits(2,:));
end