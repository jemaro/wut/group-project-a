function v = validateBoundingBox(EE, bb)
    % EE: Nx6 array with End Effector position as the first three row values
    % bb: 2x3 [xmin, ymin, zmin; xmax, ymax, zmax]
    xyz = EE(:, 1:3); % Only interested in position
    v = all((xyz >= bb(1, :)) - (xyz > bb(2, :)), 2);
end
