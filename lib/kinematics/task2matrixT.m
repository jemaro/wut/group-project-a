function T = task2matrixT(EE)
    % TASK2MATRIXT Converts a 1x6 vector with position and orientation of the
    % end effector into a 4x4 homogeneous transformation matrix
    %
    % EE: nx6 array with end effector position and orientation in the task
    % space as rows [x, y, z, gamma, beta, alpha]
    % gamma: Rotation around X axis
    % beta: Rotation around Y axis
    % alpha: Rotation around Z axis
    % Returns T: 4x4xn transformation mattrix

    cg = cos(EE(:, 4)); sg = sin(EE(:, 4));
    cb = cos(EE(:, 5)); sb = sin(EE(:, 5));
    ca = cos(EE(:, 6)); sa = sin(EE(:, 6));
    T = zeros(4, 4, size(EE, 1));
    % Set rotation matrix
    T(1, 1, :) = ca .* cb;
    T(1, 2, :) = ca .* sb .* sg - sa .* cg;
    T(1, 3, :) = ca .* sb .* cg + sa .* sg;
    T(2, 1, :) = sa .* cb;
    T(2, 2, :) = sa .* sb .* sg + ca .* cg;
    T(2, 3, :) = sa .* sb .* cg - ca .* sg;
    T(3, 1, :) = -sb;
    T(3, 2, :) = cb .* sg;
    T(3, 3, :) = cb .* cg;
    % Set position
    T(1, 4, :) = EE(:, 1);
    T(2, 4, :) = EE(:, 2);
    T(3, 4, :) = EE(:, 3);
    T(4, 4, :) = 1;
    % T = [ca.*cb,    ca.*sb.*sg - sa.*cg,    ca.*sb.*cg + sa.*sg,    EE(:, 1);
    %      sa.*cb,    sa.*sb.*sg + ca.*cg,    sa.*sb.*cg - ca.*sg,    EE(:, 2);
    %      -sb,       cb.*sg,                 cb.*cg,                 EE(:, 3);
    %      0,         0,                      0,                      1];
