function q = invKinUnique(T, q_previous, dh, config)
    % TODO docu
    % config: 1x3 vector [back, down, flip]
    % q is the accumulator of solutions, each row is a set of joint
    % solutions while each column is a different joint

    tol = 1e-8;
    [d1, a2, a3, d4, d6, qoffset] = extractKinConstants(dh);

    config = max(config, 0);
    front = 1 - 2 * config(1);
    down = front * (2 * config(2) - 1);
    noflip = 1 - 2 * config(3);

    % Preallocate output
    N = size(T, 3);
    q = NaN(N, 6);

    % Extract useful variables from the transformation matrix
    r13(:, 1) = T(1, 3, :); r23(:, 1) = T(2, 3, :); r33(:, 1) = T(3, 3, :);
    r12(:, 1) = T(1, 2, :); r22(:, 1) = T(2, 2, :); r32(:, 1) = T(3, 2, :);
    r11(:, 1) = T(1, 1, :); r21(:, 1) = T(2, 1, :); r31(:, 1) = T(3, 1, :);
    px(:, 1) = T(1, 4, :);  py(:, 1) = T(2, 4, :);  pz(:, 1) = T(3, 4, :);

    px_d6r13 = px - d6 .* r13;
    py_d6r23 = py - d6 .* r23;

    %% Joint 1


    if any((abs(px_d6r13) < tol) & (abs(py_d6r23) < tol))
        error('Robot:badinput', 'Singularity in joint 1');
    else
        try
            q(:, 1) = atan2(front * py_d6r23, front * px_d6r13);
        catch exception
            error('Robot:badinput', 'Singularity in joint 1: %s', ... 
                exception.message);
        end
    end

    %% Joint 2

    K = cos(q(:, 1)) .* px_d6r13 + sin(q(:, 1)) .* py_d6r23;
    K2 = K.^2;
    L = d6 .* r33 + d1 - pz;
    L2 = L.^2;
    M = (a2^2 - a3^2 - d4^2 + K2 + L2) ./ (2.0 * a2);

    r = sqrt(K2 + L2);

    if any(r < tol)
        error('Robot:badinput', 'Singularity in joint 2');
    else
        phi = atan2(L, K);
        phi_q2 = acos(M ./ r);

        if ~all(isreal(phi_q2))
            % TODO specify which is the cause of the issue, which T
            error('Robot:badinput', 'Point not reachable');
        end

        q(:, 2) = wrapToPi(phi + down .* phi_q2);

    end

    %% Joint 3

    num = cos(q(:, 2)) .* K + sin(q(:, 2)) .* L - a2;
    den = cos(q(:, 2)) .* L - sin(q(:, 2)) .* K;
    q(:, 3) = wrapToPi(atan2(a3, d4) - atan2(num, den));

    %% Joint 5

    A = cos(q(:, 1)) .* r13 + sin(q(:, 1)) .* r23;
    q23 = q(:, 2) + q(:, 3);
    q(:, 5) = noflip .* wrapToPi(acos(-cos(q23) .* r33 - sin(q23) .* A));

    %% Joint 4

    % If q5 = +- pi/2 there is a singularity
    q5singular = abs(abs(q(:, 4)) - pi / 2) < tol;

    if ~all(q5singular)
        B = cos(q(:, 1)) .* r23 - sin(q(:, 1)) .* r13;
        num = sign(q(~q5singular, 5)) .* B;
        den = sign(q(~q5singular, 5)) .* (-cos(q23) .* A + sin(q23) .* r33);

        try
            q(~q5singular, 4) = atan2(num, den);
        catch
            warning('B is close to 0')
            disp(B)
            q(~q5singular, 4) = atan2(img2nan(num), img2nan(den));
        end

    else
        warning('Singularity in thetat 5')
    end

    % Assign singular
    q(:, 4) = fillmissing(q(:, 4), 'previous');
    q(:, 4) = fillmissing(q(:, 4), 'constant', q_previous(4));

    %% Joint 6
    % TODO simplify
    c_1 = cos(q(:, 1)); s_1 = sin(q(:, 1));
    c_2 = cos(q(:, 2)); s_2 = sin(q(:, 2));
    c_3 = cos(q(:, 3)); s_3 = sin(q(:, 3));
    c_4 = cos(q(:, 4)); s_4 = sin(q(:, 4));
    c_5 = cos(q(:, 5)); s_5 = sin(q(:, 5));

    num = r12.*(c_5.*s_1.*s_4+c_1.*(c_2.*(c_3.*c_4.*c_5-s_3.*s_5)-s_2.*(c_4.*c_5.*s_3+c_3.*s_5)))-r22.*(c_4.*c_5.*s_1.*s_2.*s_3+c_1.*c_5.*s_4+c_3.*s_1.*s_2.*s_5+c_2.*s_1.*(s_3.*s_5-c_3.*c_4.*c_5))+r32.*(s_3.*(s_2.*s_5-c_2.*c_4.*c_5)-c_3.*(c_4.*c_5.*s_2+c_2.*s_5));
    num = -num;
    den = r11.*(c_5.*s_1.*s_4+c_1.*(c_2.*(c_3.*c_4.*c_5-s_3.*s_5)-s_2.*(c_4.*c_5.*s_3+c_3.*s_5)))-r21.*(c_4.*c_5.*s_1.*s_2.*s_3+c_1.*c_5.*s_4+c_3.*s_1.*s_2.*s_5+c_2.*s_1.*(s_3.*s_5-c_3.*c_4.*c_5))+r31.*(s_3.*(s_2.*s_5-c_2.*c_4.*c_5)-c_3.*(c_4.*c_5.*s_2+c_2.*s_5));
    
    q(:, 6) = atan2(num, den);

    % Remove joint offsets
    q = wrapToPi(real(q) - qoffset);
end

function array = img2nan(array)
    array(array ~= real(array)) = NaN;
end
