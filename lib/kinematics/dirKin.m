function Tn = dirKin(q, dh)
    % q: joint values [q1 q2 q3 q4 q5 q6]
    % dh: matrix 6x4 [q_offset(:) d(:) a(:) alpha(:)]

    % Add joint offsets
    qoffset(1, :) = dh(:, 1);
    q = q + qoffset;

    % TODO harcode T6 for speed
    % TODO handle theta offsets dh(:, 1)
    Tn = zeros(4, 4, 6);
    Tn(:, :, 1) = matrixT(dh(1, 3), dh(1, 4), dh(1, 2), q(1));

    for k = 2:size(dh, 1)
        Tn(:, :, k) = Tn(:, :, k-1)*matrixT(dh(k, 3), dh(k, 4), dh(k, 2), q(k));
    end
