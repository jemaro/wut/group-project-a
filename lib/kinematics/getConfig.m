function config = getConfig(q, T, dh)
    % GETCONFIG calculates the configuration boolean vector from the joint
    % angle values and the end effector position and orientation
    % 
    % q: 1x6 vector of joint angle values [q1 q2 q3 q4 q5 q6]
    % T: 4x4 homogeneous transformation of the end effector. If not provided it
    % will be calculated solving the direct kinematic problem. 
    % dh: 6x4 Denavit-Hartenberg parameters matrix: [qoffset d a alpha; ...]
    % 
    % config: Robot configuration boolean vector [back down flip]
    % TODO define better
    % back: Whether the shoulder is inverted. Defaults to 0, which is shoulder
    % pointing to the front.
    % down: Whether the elbow is in down position. Defaults to 0.
    % flip: Whether the wrist is flipped. Defaults to 0.
    % 
    % Execute run_tests 'show_config' to visualize the different options
    % 
    % Usage:
    % config = getConfig(q, T, dh)
    % config = getConfig(q, EE, dh)
    % config = getConfig(q, dh)

    if nargin == 2
        dh = T;
        T = dirKinEE(q, dh);
    elseif ~isequal(size(T), [4 4])
        T = task2matrixT(T);
    end

    [d1, ~, ~, ~, d6, qoffset] = extractKinConstants(dh);

    q = q + qoffset;

    px_d6r13 = T(1, 4) - d6 * T(1, 3);
    py_d6r23 = T(2, 4) - d6 * T(2, 3);

    back = abs(angdiff(q(1), atan2(py_d6r23, px_d6r13))) > pi / 2;
    K = cos(q(1)) * px_d6r13 + sin(q(1)) * py_d6r23;
    L = d6 * T(3, 3) + d1 - T(3, 4);

    down = angdiff(q(2), atan2(L, K)) > 0;
    down = (down || back) && ~(down && back);
    flip = max(-sign(sin(q(5))), 0);

    config = [back down flip];
end
