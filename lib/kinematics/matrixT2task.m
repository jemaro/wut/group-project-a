function EE = matrixT2task(T)
    % Extract angles and position from the transformation matrix
    % Returns EE = [x, y, z, gamma, beta, alpha]
    % gamma: Rotation around X axis
    % beta: Rotation around Y axis
    % alpha: Rotation around Z axis

    % * I don't know why but it seems that cheking the singularity is not
    % * necessary. The tests reveal that it leads to the same results
    % if T(3, 1) == -1 % Check beta = +90� by checking sibetata) = +1
    %     a = 0; b = pi / 2;
    %     g = atan2(T(1, 2), T(1, 3));
    % elseif T(3, 1) == 1 % Check beta = -90� by checking sibetata) = -1
    %     a = 0; b = -pi / 2;
    %     g = -atan2(T(1, 2), T(2, 2));
    % else
    %     g = atan2(T(3, 2), T(3, 3));
    %     b = atan2(-T(3, 1), sqrt(T(3, 2).^2 + T(3, 3).^2));
    %     a = atan2(T(2, 1), T(1, 1));
    % end

    g(:) = atan2(T(3, 2, :), T(3, 3, :));
    b(:) = atan2(-T(3, 1, :), sqrt(T(3, 2, :).^2 + T(3, 3, :).^2));
    a(:) = atan2(T(2, 1, :), T(1, 1, :));
    pos(:, :) = T(1:3, 4, :);

    EE = [pos; g; b; a;]';
