function theta = randPi(size)
    theta = wrapToPi(2*pi*rand(size));