function q = task2joint(EE, q_previous, dh, config)
    % EE: Can also accept an array of transformation matrices

    % The solution will be selected based on the configuration

    % If it is not an array of End Effector position and orientation values
    if size(EE, 2) ~= 6
        T = EE;
    else
        T = task2matrixT(EE);
    end

    q = invKinUnique(T, q_previous, dh, config);
end
