function EE = joint2task(q, dh)
    % q: Nx6 array of joint values [q1, q2, q3, q4, q5, q6]
    % EE: Nx6 array of end effector task space position and orientation values
    %   [x, y, z, gamma, beta, alpha]
    [d1, a2, a3, d4, d6, qoffset] = extractKinConstants(dh);
    % Add joint offsets
    q = q + qoffset;

    % T = dirKinEE(q, dh);
    % EE = matrixT2task(T);

    % Hardcoded for efficiency. The following code is a mixture of dirKinEE and
    % matrixT2task. Not all elements of the homogeneous transformation matrix
    % need to be calculated in in order to get the postition and orientation of
    % the end effector
    c1 = cos(q(:, 1)); s1 = sin(q(:, 1));
    c2 = cos(q(:, 2)); s2 = sin(q(:, 2));
    c3 = cos(q(:, 3)); s3 = sin(q(:, 3));
    c23 = cos(q(:, 2) + q(:, 3)); s23 = sin(q(:, 2) + q(:, 3));
    c4 = cos(q(:, 4)); s4 = sin(q(:, 4));
    c5 = cos(q(:, 5)); s5 = sin(q(:, 5));
    c6 = cos(q(:, 6)); s6 = sin(q(:, 6));
    r11 = s1 .* (c5 .* c6 .* s4 + c4 .* s6) ...
        + c1 .* (c23 .* (c4 .* c5 .* c6 - s4 .* s6) - c6 .* s5 .* s23);
    r21 = c6 .* (c5 .* (c2 .* c3 .* c4 .* s1 - c4 .* s2 .* s3 .* s1 ...
        - c1 .* s4) - s1 .* s5 .* s23) - s6 .* (s1 .* s4 .* c23 + c1 .* c4);
    r31 = s4 .* s6 .* s23 - c6 .* (c3 .* (c4 .* c5 .* s2 + c2 .* s5) ...
        + s3 .* (c2 .* c4 .* c5 - s2 .* s5));
    r32 = c3 .* (c6 .* s2 .* s4 + s6 .* (c4 .* c5 .* s2 + c2 .* s5)) ...
        + s3 .* (c2 .* (c6 .* s4 + c4 .* c5 .* s6) - s2 .* s5 .* s6);
    r33 = s2 .* (c5 .* s3 + c3 .* c4 .* s5) ...
        + c2 .* (c4 .* s3 .* s5 - c3 .* c5);
    x = c1 .* (c23 .* (a3 - c4 .* d6 .* s5) ...
        - s23 .* (c5 .* d6 + d4) + a2 .* c2) - d6 .* s1 .* s4 .* s5;
    y = c2 .* s1 .* (c3 .* (a3 - c4 .* d6 .* s5) + a2 ...
        - s3 .* (c5 .* d6 + d4)) - a3 .* s1 .* s3 .* s2 ...
        - c3 .* s1 .* s2 .* (c5 .* d6 + d4) ...
        + c4 .* d6 .* s1 .* s3 .* s5 .* s2 + c1 .* d6 .* s4 .* s5;
    z = s2 .* (c3 .* (c4 .* d6 .* s5 - a3) - a2 ...
        + s3 .* (c5 .* d6 + d4)) - c2 .* (s3 .* (a3 - c4 .* d6 .* s5) ...
        + c3 .* (c5 .* d6 + d4)) + d1;

    g = atan2(r32, r33);
    b = atan2(-r31, sqrt(r32.^2 + r33.^2));
    a = atan2(r21, r11);

    EE = [x y z g b a];
