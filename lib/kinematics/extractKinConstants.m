function [d1, a2, a3, d4, d6, qoffset] = extractKinConstants(dh)
    % Extracts the constants used in the direct and inverse kinematic problem
    % from a Denavit-Hartenberg parameters array.
    % dh: Nx4 [theta_offset, d, a, alpha]
    % d1, a2 , a3, d4, d6: double
    % qoffset: 1xN [q1offset, q2offset, q3offset, q4offset, q5offset, q6offset]
    qoffset(1, :) = dh(:, 1);
    d1 = dh(1, 2);
    a2 = dh(3, 3);
    a3 = dh(4, 3);
    d4 = dh(4, 2);
    d6 = dh(6, 2);
end
