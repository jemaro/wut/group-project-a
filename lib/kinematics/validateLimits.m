function v = validateLimits(q, limits)
    % q: Nx6 vector of joint values [q1, q2, q3, q4, q5, q6]
    % limits: 2x6 array of joint limit values 
    % [q1min, q2min, q3min, q4min, q5min, q6min;
    %  q1max, q2max, q3max, q4max, q5max, q6max]
    limited_joints = all(~isnan(limits), 1);
    q = q(:, limited_joints);
    limits = limits(:, limited_joints);
    v = all((q >= limits(1, :)) - (q > limits(2, :)), 2);
end