function q = invKin(T, q_previous, dh)
    % q is the accumulator of solutions, each row is a set of joint
    % solutions while each column is a different joint
    tol = 1e-5;
    [d1, a2, a3, d4, d6, qoffset] = extractKinConstants(dh);

    %% Joint 1

    d6r13_px = d6 * T(1, 3) - T(1, 4);
    d6r23_py = d6 * T(2, 3) - T(2, 4);

    if abs(d6r13_px) < tol && abs(d6r23_py) < tol
        error('Robot:badinput', 'Singularity in joint 1')
    else
        % ? Is there a better way, shouldn't q1 be a unique solution ?
        q = [
            atan2(d6r23_py, d6r13_px)
            atan2(-d6r23_py, -d6r13_px)
            ];
    end

    %% Joint 2

    K = -cos(q) * d6r13_px - sin(q) * d6r23_py;
    L = d6 * T(3, 3) + d1 - T(3, 4);
    M = (a2^2 - a3^2 - d4^2 + K.^2 + L.^2) / (2.0 * a2);

    r = sqrt(K.^2 + L.^2);

    if r < tol
        error('Robot:badinput', 'Singularity in joint 2')
    else
        phi = atan2(L, K);
        phi_q2 = acos(M ./ r);

        if ~isreal(phi_q2)
            % fprintf('T = ');
            % disp(T);
            error('Robot:badinput', 'Point not reachable');
        end

        q2 = wrapToPi([phi - phi_q2; phi + phi_q2]);
        q = [fitVector(q, q2) q2];
    end

    %% Joint 3

    num = cos(q(:, 2)) .* fitVector(K, q) + sin(q(:, 2)) .* L - a2;
    den = cos(q(:, 2)) .* L - sin(q(:, 2)) .* fitVector(K, q);
    q(:, 3) = wrapToPi(atan2(a3, d4) - atan2(num, den));

    %% Joint 5

    A = cos(q(:, 1)) * T(1, 3) + sin(q(:, 1)) * T(2, 3);
    q23 = q(:, 2) + q(:, 3);
    q5 = acos(-cos(q23) .* T(3, 3) - sin(q23) .* A);
    q5 = [q5; -q5];
    q = [fitVector(q, q5) zeros(size(q5)) q5];

    %% Joint 4

    B = cos(q(:, 1)) * T(2, 3) - sin(q(:, 1)) * T(1, 3);
    % Resize constants
    q23 = fitVector(q23, q);
    A = fitVector(A, q);
    num = sign(q5) .* B;
    den = sign(q5) .* (-cos(q23) .* A + sin(q23) .* T(3, 3));

    try
        q4 = atan2(num, den);
    catch
        warning('B is close to 0')
        disp(B)
        q4 = atan2(img2nan(num), img2nan(den));
    end

    % If s5 = 0 (lower than a thereshold -here 1e-05-) there is a singularity
    q5singular = abs(sin(q5)) < 1e-05;
    % * Not sure about this, I would keep it simpler and have another function
    % * in top of this one that selects a single solution and sets the NaN
    % * of q(4) as the previous solution
    if sum(q5singular) > 0
        % q4(q5singular) = NaN;
        q4(q5singular) = q_previous(4);
    end

    q(:, 4) = q4;

    %% Joint 6

    % Resize constants
    q23 = fitVector(q23, q);
    Y = sin(q(:, 4)) .* sin(q23);
    r = sqrt(T(3, 1).^2 + T(3, 2).^2);
    phi = atan2(T(3, 1), T(3, 2));
    phi_q6 = acos(Y ./ r);
    % ! This can also fail because of imaginary numbers
    q6 = wrapToPi([phi - phi_q6; phi + phi_q6]);
    q = fitVector(q, q6);

    % If s5 = 0 there is a singularity, q6 is reflected as q6+q4
    if sum(q5singular) > 0

        warning('Singularity on joints 4 and 6! s5 = 0 , theta4 taken as previous value')
        q5singular = fitVector(q5singular, q);
        num = T(2, 1) * cos(q(:, 1)) - T(1, 1) * sin(q(:, 1));
        den = T(2, 2) * cos(q(:, 1)) - T(1, 2) * sin(q(:, 1));
        q46 = atan2(num, den);
        % warning('theta4 and theta6 are coupled, the result on q6 are from the combination of both angles,when a value for theta4 is obtained, theta6 can be derived')
        q6(q5singular) = q46(q5singular);
        q4new = fitVector(q4, q6);
        q6 = [q6; q6];
        q6([q5singular; q5singular]) = [q46(q5singular) + q4new(q5singular); q46(q5singular) - q4new(q5singular)];
    end

    q = [fitVector(q, q6) q6];

    % Remove joint offsets
    q = wrapToPi(real(q) - qoffset);
end

function vector = fitVector(vector, template)
    vector = repmat(vector, [size(template, 1) / size(vector, 1) 1]);
end

function array = img2nan(array)
    array(array ~= real(array)) = NaN;
end
