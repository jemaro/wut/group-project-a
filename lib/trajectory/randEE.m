function EE = randEE(bb)
    % bb: Task space bounding box: [xmin, ymin, zmin; xmax, ymax, zmax]
    EE = [bb(1, :) + (bb(2, :) - bb(1, :)) .* rand(1, 3), randPi([1 3])];
end