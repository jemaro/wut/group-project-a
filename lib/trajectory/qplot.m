function qplot(t, q, states, statechngidxs)
    % TODO add input parser and manage with axes handle
    if nargin == 1 || nargin == 3
        if nargin == 3
            statechngidxs = states;
            states = q;
        end
        q = t;
        t = (1:numrows(q))';
        xlabel_ = 'Time';
    else
        xlabel_ = 'Time (s)';
    end

    hold on
    plot(t, q(:,1:3))
    plot(t, q(:,4:6), '--')
    grid on
    xlabel(xlabel_)
    ylabel('Joint coordinates (rad,m)')

    % Plot vertical lines. Could be improved
    if nargin > 2
        len_idxs = length(statechngidxs);
        len_states = length(states);
        if len_idxs + 1 == len_states
            statechngidxs = [1 statechngidxs];
        elseif len_idxs ~= len_states
            error('States and indexes do not match')
        end

        for k = 1:len_states
            xline(statechngidxs(k), '-', states{k});
        end
    end

    legend('q1', 'q2', 'q3', 'q4', 'q5', 'q6', 'Location', 'northeastoutside')
    hold off

    xlim([t(1), t(end)]);
    ylim([-pi, pi]);
end