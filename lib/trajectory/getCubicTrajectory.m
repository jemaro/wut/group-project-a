function [qq, t] = getCubicTrajectory(s0, v0, sf, vf, t0, tf, timestep)
    % s0, v0, sf and vf are 1xM vectors
    % t0, tf and timestep are doubles
    % qq: NxM array of trajectory values
    % TODO linear system could be pre-solved by setting t0=0 and tf=tf-t0
    A = [1, t0,  t0^2,    t0^3
         0,  1,  2*t0,  3*t0^2;
         1, tf,  tf^2,    tf^3;
         0,  1,  2*tf,  3*tf^2];
    b = [s0; v0; sf; vf];
    a = A \ b;
    t = (t0:timestep:tf)';
    qq = [ones(length(t), 1) t t.^2 t.^3]*a;
end