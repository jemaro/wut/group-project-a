function EEplot(t, EE, states, statechngidxs)
    % t: time vector, optional

    if nargin == 1 || nargin == 3
        if nargin == 3
            statechngidxs = states;
            states = EE;
        end
        EE = t;
        t = (1:numrows(EE))';
        xlabel_ = 'Time';
    else
        xlabel_ = 'Time (s)';
    end

    yyaxis left
    plot(t, EE(:, 1), 'b-', t, EE(:, 2), 'b--', t, EE(:, 3), 'b.')
    ylabel('End Effector position')
    yyaxis right
    plot(t, EE(:, 4), 'r-', t, EE(:, 5), 'r--', t, EE(:, 6), 'r.')
    ylabel('End Effector orientation (rad)')
    grid on
    xlabel(xlabel_)
    
    % Plot vertical lines. Could be improved
    if nargin > 2
        len_idxs = length(statechngidxs);
        len_states = length(states);
        if len_idxs + 1 == len_states
            statechngidxs = [1 statechngidxs];
        elseif len_idxs ~= len_states
            error('States and indexes do not match')
        end

        for k = 1:len_states
            xline(statechngidxs(k), '-', states{k});
        end
    end

    legend('x', 'y', 'z', '\gamma', '\beta', '\alpha', ...
        'Location', 'northeastoutside')

    hold off

    xlim([t(1), t(end)]);
end
