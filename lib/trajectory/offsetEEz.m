function EE = offsetEEz(EE, d)
    % OFFSETEEZ modifies the End Effector position in the direction of the End
    % Effector z axis by a distance d. The end effector orientation will not be
    % modified, but its position will.
    % 
    % EE: End Effector position and orientation [x, y, z, gamma, beta, alpha]
    % d: Offset distance that will be applied to EE

    cg = cos(EE(:, 4)); sg = sin(EE(:, 4));
    cb = cos(EE(:, 5)); sb = sin(EE(:, 5));
    ca = cos(EE(:, 6)); sa = sin(EE(:, 6));
    r13 = ca .* sb .* cg + sa .* sg;
    r23 = sa .* sb .* cg - ca .* sg;
    r33 = cb .* cg;

    EE(:, 1:3) = EE(:, 1:3) + d.*[r13 r23 r33];
end