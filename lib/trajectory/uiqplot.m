function uiqplot(q, ax)

    t = (1:numrows(q))';
    hold(ax, 'off')
    plot(ax, t, q(:,1:3))
    hold(ax, 'on')
    plot(ax, t, q(:,4:6), '--')
    hold(ax, 'off')

    grid(ax, 'on')
    ylabel(ax, 'Joint coordinates (rad)')
    xlabel(ax, sprintf('Last %d movement steps', length(t)))
    xticks(ax, [])
    legend(ax, 'q1', 'q2', 'q3', 'q4', 'q5', 'q6', ... 
        'Location', 'northeastoutside')

    xlim(ax, [t(1), t(end)]);
    ylim(ax, [-pi, pi]);
    set(ax, 'Color', 'w');
end