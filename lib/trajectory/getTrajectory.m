function qq = getTrajectory(method, q0, v0, qf, vf, t0, tf, timestep)
    % q0, v0, qf and vf are 1xM vectors
    % t0, tf and timestep are doubles
    % qq: NxM array of trajectory values
    % method: 'c' for cubic, 'l' for LSPB
    m = lower(method(1));
    if m == 'c'
        qq = getCubicTrajectory(q0, v0, qf, vf, t0, tf, timestep);
    elseif m == 'l'
        qq = getLSPBTrajectory(q0, v0, qf, vf, t0, tf, timestep);
    else
        error('Method %s is not implemented', method);
    end
