N = 4;
max_attempts = 100;
robot = Robot('simulate', false);
% Speed up the tests
robot.task_vmax_lin = 0.8; % [task space units/s]
robot.task_vmax_ang = 0.8; % [rad/s]
figure(1)
clf;

x = [-10: 0.1 :10];
y = [-10: 0.1: 10];
z = zeros(1,length(x));

for n = 1:length(x)
    x = x(n); y = y(n); z = z(n);
    % Select random points until one is valid
    attempt = 0;

    while 1
        previous = [x y z randPi([1 3])];

        if validateLimits(previous, robot.qlimits)
            break;
        end

    end

    while 1
        attempt = attempt + 1;
        EEpick = randEE(robot.task_bb);
        EEplace = randEE(robot.task_bb);

        try
            [qq, changeidx] = robot.pick_and_place(EEpick, EEplace);
            EEpick
            EEplace
            break;
        catch exception

            if (strcmp(exception.identifier, 'Robot:badinput'))
                continue;
            else
                rethrow(exception);
            end

        end

        if attempt > max_attempts
            rethrow(exception);
        end

    end

    subplot(2, 2, 2);
    cla(qax);
    qplot(qq, phases, changeidx);
    subplot(2, 2, 4);
    cla(EEax);
    EEplot(robot.joint2task(qq), phases, changeidx);
    pause(0.2);
end