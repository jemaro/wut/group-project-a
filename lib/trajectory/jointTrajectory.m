function qq = jointTrajectory(method, qlimits, q0, v0, qf, vf, vmax, timestep)

    % vmax: rad/s

    % Check goal is in limits, assume origin is correct
    assert(validateLimits(qf, qlimits), 'Robot:badinput', ...
        'Objective is out of joint limits');
    
    % Compute duration according to the maximum joint angular velocity
    % ! I think it does not hold. It doesn't take into account initial
    % ! velocities and acceleration periods
    duration = max(abs(angdiff(q0, qf)))/vmax; % [s]

    % If the joint doesn't have limits, it can cross +-pi. Therefore it can be
    % adjusted to use the shortest path.
    qdiff = q0-qf;
    free_joints = all(isnan(qlimits), 1);
    q0(free_joints) = q0(free_joints) ... 
        - (abs(qdiff(free_joints)) > pi).*2.*pi.*sign(qdiff(free_joints));

    % TODO get the trajectory according to the method
    if method(1) == 'c'
        qq = getCubicTrajectory(q0, v0, qf, vf, 0, duration, timestep);
    elseif method(1) == 'l'
        qq = getLSPBTrajectory(q0, v0, qf, vf, 0, duration, timestep);
    end
   

    % Wrap output back to [-pi, +pi], otherwise the plot explodes
    qq = wrapToPi(qq);
end