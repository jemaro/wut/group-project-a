function [qq, configf, startidx, EEtraj] = taskTrajectory(... 
    method, qlimits, config0, q0, vEE0, EEf, vEEf, ... 
    vmax_lin, vmax_ang, timestep, dh)
    % vmax_lin: task space units/s
    % vmax_ang: rad/s

    % Compute the initial task space coordinates
    EE0 = joint2task(q0, dh);
    T0 = task2matrixT(EE0);
    p0 = EE0(1:3);
    r0 = UnitQuaternion(T0);
    Tf = task2matrixT(EEf);
    pf = EEf(1:3);
    rf = UnitQuaternion(Tf);

    % Compute duration according to the maximum velocities
    % ! I think it does not hold. It doesn't take into account initial
    % ! velocities and acceleration periods
    eulerDiff = angdiff(EE0(4:6), EEf(4:6));
    abs_eulerDiff = abs(eulerDiff);
    duration = max([pdist([p0; pf]) / vmax_lin, abs_eulerDiff / vmax_ang]); % [s]

    % TODO compute the trajectory according to method
    try
        s = linspace(0, 1, duration / timestep);
    catch exception
        disp(duration / timestep)
        rethrow(exception)
    end
    len_s = length(s);
    TT = zeros(4, 4, len_s);
    TT(4, 4, :) = 1;

    for k = 1:len_s
        rk = r0.interp(rf, s(k)); % Interpolate orientation
        pr = p0 * (1 - s(k)) + s(k) * pf; % Interpolate position
        TT(1:3, 1:3, k) = rk.R;
        TT(1:3, 4, k) = pr';
    end


    % Inverse kinematics the trajectory
    OK = 0;
    for k=0:7 % Try all configurations, ordered by preference
        configf = index2config(k);
        qq = task2joint(TT, q0, dh, configf);
        if all(validateLimits(qq, qlimits))
            OK = 1;
            break; 
        end
    end
    if ~OK
        % TODO improve error
        error('Robot:badinput', ... 
            'Could not find a trajectory contained within joint limits')
    end

    % Change the configuration before the trajectory if it is needed
    if ~isequal(config0, configf)
        vzero = zeros(1, 6);
        pre_qq = jointTrajectory(method, qlimits, ... 
            q0, vzero, qq(1, :), vzero, vmax_ang, timestep);
        startidx = size(pre_qq, 1) + 1;
        qq = vertcat(pre_qq, qq);
    else
        startidx = 1;
    end

    % Output used for debugging purposes
    if nargin > 3
        EEtraj = matrixT2task(TT);
    end
end

function config = index2config(k)
    config = zeros(1, 3);
    config(1, 3) = mod(k, 2); k = floor(k/2);
    config(1, 2) = mod(k, 2); 
    config(1, 1) = floor(k/2);
end