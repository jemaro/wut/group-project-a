% the robot will perform the approach movement in a linear trajectory
function [trajectory, invKinSolution] = approach(previous, pstart, pfinish, ti, tf, timestep, qlimits)

    % TODO remove: Workaround while combining this function with Robot
    dh = Robot().dh;

    t = [ti:timestep:tf];
    n = length(pstart);
    trajectory = zeros(n, length(t));

    trajectory(1, :) = pstart(1) * ones(1, length(t));
    trajectory(2, :) = pstart(2) * ones(1, length(t));
    trajectory(3, :) = linspace(pstart(3), pfinish(3), length(t));
    
    orientation = pstart(4:6);

    %% Get the IKP solution for each step of the trajectory
    % The trajectory from generateCubicTrajectory() is computed in the task
    % space, it should be taken to joint space solving the IKP.
    nSteps = length(trajectory);
    invKinSolution = zeros(6, nSteps);

    for k=1:length(trajectory)
        EE = [trajectory(1:3,k); orientation'];
        invKinSolution(:,k) = task2joint(EE', previous, dh, qlimits);
        previous = invKinSolution(:,k)';
    end

end
