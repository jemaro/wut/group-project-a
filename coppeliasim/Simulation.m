classdef Simulation < handle

    properties
        htimer; % Handle to the simulator step timer
        api;
        clientID;
        hjoints; % Identifiers of the manipulator joints
        hgripper; % Identifier of the gripper_close joint
        % Input
        control_fcn;
        control_args;
        timestep;
        timefactor;
        start_joints;
    end

    methods

        function obj = Simulation(varargin)
            % RUN_SIMULATION(control_fcn)
            % RUN_SIMULATION(control_fcn, 'timestep', timestep)
            % RUN_SIMULATION(control_fcn, 'control_args', {arg1, arg2})
            % RUN_SIMULATION(control_fcn, 'start_joints', [0 0 0 0 0 0])
            %
            % control_fcn: Handle to the control function, this function
            % described below
            % timestep: Seconds between simulation steps
            % control_args: Cell array that will be passed to the control
            % function after the required arguments
            %
            % [vq,gripper_close] = CONTROL_FCN(q,gripper_closed,t,control_args)
            % q: Current joint angle values in radians
            % gripper_closed: Current position of the gripper being 0
            % completely open and 1 completely closed
            % t: Simulation time in ms
            % control_args: Extra arguments provided by RUN_SIMULATION
            % vq: Desired joint velocities response
            % gripper_close: Whether to close or to open the gripper. 0 will
            % open the gripper or keep it open while 1 will close the gripper
            % or keep it closed.

            % Parse input
            p = inputParser;

            isfunction = @(x) isa(x, 'function_handle');
            addRequired(p, 'control_fcn', isfunction);

            addOptional(p, 'timestep', 0.050); % 50 ms
            addOptional(p, 'timefactor', 2); % Real time / Simulation time
            addOptional(p, 'control_args', {});
            addOptional(p, 'start_joints', [0 0 0 0 0 0]);

            parse(p, varargin{:});

            % Assign the parsed input to the class properties
            obj.control_fcn = p.Results.control_fcn;
            obj.timestep = p.Results.timestep;
            obj.timefactor = p.Results.timefactor;
            obj.control_args = p.Results.control_args;
            obj.start_joints = p.Results.start_joints;

            % Initialize API
            api = remApi('remoteApi'); % 'remoteApi' is the name of the library
            api.simxFinish(-1); % Close previous connections
            % Connect to Coppeliasim
            clientID = api.simxStart('127.0.0.1', ... server
            19997, ... port
            true, ... wait untiil connected
            true, ... do not reconnect once disconnected
            2000, ... timeout in ms
            5); % comm thread cycle in ms
            assert(clientID >= 0, 'Simulation:missing_server', ...
                'Could not find a running instance of Coppeliasim');
            fprintf('Connection %d to remote API server open.\n', clientID);

            % Set the simulation in synchronous mode
            errcode = api.simxSynchronous(clientID, true);
            assert(errcode == api.simx_error_noerror, ... 
                'Simulation:initialization', ...
                'Could not set the simulation in synchronous mode');

            % Operation mode of the following initializations
            % https://www.coppeliarobotics.com/helpFiles/en/remoteApiConstants.htm
            wait_completed = api.simx_opmode_oneshot_wait;

            % Initialize joints
            obj.hjoints = zeros(1, 6);

            for k = 1:length(obj.hjoints)
                % Get and save the handle to the joint to access it
                [errcode, hjoint] = api.simxGetObjectHandle(...
                    clientID, sprintf('q%d', k), wait_completed);
                assert(errcode == api.simx_error_noerror, ...
                    'Simulation:wrong_scene', ...
                    'Could not find joint "q%d"', k);
                obj.hjoints(k) = hjoint;
                % Initialize the streaming of joint position that will be used
                % afterwards at each simulation step
                [errcode, ~] = api.simxGetJointPosition(...
                    clientID, hjoint, api.simx_opmode_streaming);
                assert(errcode <= api.simx_error_novalue_flag, ...
                    'Simulation:initialization', ...
                    'Could not get q%d position. Error code %d', k, errcode);
                % Set the start joint position
                errcode = api.simxSetJointPosition(clientID, hjoint, ...
                    obj.start_joints(k), wait_completed);
                assert(errcode == api.simx_error_noerror, ...
                    'Simulation:initialization', ...
                    'Could not set q%d to position %d. Error code %d', ...
                    k, obj.start_joints(k));
                % Reset start target velocity
                errcode = api.simxSetJointTargetVelocity(clientID, hjoint, ...
                    0, wait_completed);
                assert(errcode == api.simx_error_noerror, ...
                    'Simulation:initialization', ...
                    'Could not reset q%d velocity. Error code %d', ...
                    k, obj.start_joints(k));
            end

            % Initialize gripper
            [errcode, hgripper] = api.simxGetObjectHandle(...
                clientID, 'Gripper_centerJoint', wait_completed);
            assert(errcode == api.simx_error_noerror, ...
                'Simulation:initialization', ... 
                'Could not find "Gripper_centerJoint"');
            obj.hgripper = hgripper;
            % Initialize the streaming of gripper position
            [errcode, ~] = api.simxGetJointPosition(...
                clientID, hgripper, api.simx_opmode_streaming);
            assert(errcode <= api.simx_error_novalue_flag, ...
                'Simulation:initialization', ...
                'Could not get gripper position. Error code %d', errcode);
            % Set the gripper to open at the beggining
            errcode = api.simxSetIntegerSignal(clientID, 'Gripper_close', ...
                0, wait_completed);
            assert(errcode == api.simx_error_noerror, ... 
                'Simulation:initialization', ...
                'Could not set gripper signal to closed. Error code %d', ... 
                errcode);
            
            % Save the API connection as a class property
            obj.api = api; obj.clientID = clientID;
        end

        function start(obj)
            % Set up the timer for steps in the simulation
            obj.htimer = timer('BusyMode', 'drop', ... 
                'ExecutionMode', 'fixedRate', 'Name', 'SimulationStep', ... 
                'TimerFcn', @(~, ~)obj.step(), ...
                'Tag', 'SimulationStep', ... 
                'Period', obj.timefactor*obj.timestep, ...
                'ErrorFcn', @(t, ~)obj.finish(t));

            % Start simulation
            obj.api.simxStartSimulation(... 
                obj.clientID, obj.api.simx_opmode_oneshot_wait);

            % Start the timer
            start(obj.htimer);
        end

        function step(obj)
            opmode = obj.api.simx_opmode_buffer;

            % Get joint encoders
            q = zeros(1, 6);

            for k = 1:length(obj.hjoints)
                [errcode, value] = obj.api.simxGetJointPosition(...
                    obj.clientID, obj.hjoints(k), opmode);
                assert(errcode == obj.api.simx_error_noerror, ... 
                    'Simulation:step', ...
                    'Could not get q%d value. Error code %d', k, errcode)
                q(k) = value;
            end

            % Get gripper position
            [errcode, value] = obj.api.simxGetJointPosition(...
                obj.clientID, obj.hgripper, opmode);
            assert(errcode == obj.api.simx_error_noerror, ... 
                'Simulation:step', ...
                'Could not get gripper position. Error code %d', errcode)
            % Whether the gripper is fully closed or not
            gripper_closed = value / -0.017; 

            % TODO Check if simulation is running
            time = obj.api.simxGetLastCmdTime(obj.clientID);

            % Calculate joint response
            [vq, gripper_close] = obj.control_fcn(q, gripper_closed, ...
            time, obj.control_args{:}); % Simulation time and extra arguments

            % Apply response
            for k = 1:length(obj.hjoints)
                errcode = obj.api.simxSetJointTargetVelocity(obj.clientID, ... 
                    obj.hjoints(k), vq(k), obj.api.simx_opmode_oneshot);
                assert(errcode <= obj.api.simx_error_novalue_flag, ...
                    'Simulation:step', ...
                    'Could not set q%d velocity to %d. Error code %d', ...
                    k, vq(k), errcode);
            end

            errcode = obj.api.simxSetIntegerSignal(obj.clientID, ... 
                'Gripper_close', gripper_close, obj.api.simx_opmode_oneshot);
            assert(errcode <= obj.api.simx_error_novalue_flag, ... 
                'Simulation:step', ...
                'Could not set gripper signal to %d. Error code %d', ...
                gripper_close, errcode);

            % Trigger next step
            obj.api.simxSynchronousTrigger(obj.clientID);
        end

        function finish(obj, htimer)
            if nargin == 1
                htimer = obj.htimer;
            end

            disp('Stopping simulator steps')
            stop(htimer);
            delete(htimer);
            fprintf('Closing connection %d.\n', obj.clientID);
            obj.api.simxStopSimulation(... 
                obj.clientID, obj.api.simx_opmode_oneshot_wait);
            obj.api.simxFinish(obj.clientID);
            % You may need to comment next line if it causes Matlab to crash
            obj.api.delete(); 
            disp('Program ended');
        end

    end

end
