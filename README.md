# Group Project A

Usage in `MATLAB` console:

+ Run a prepared pick and place scenario. Check [how to
  simulate](#how-to-simulate) and open the scene
  [robot_pick_and_place.ttt](coppeliasim/robot_pick_and_place.ttt) in order to
  pick a simulated object.
```
run_tests 'pick_and_place'
```

+ Execute visualization app:
```
run_visualization
```

# How to simulate
The simulator choice is Coppeliasim. Concretely its EDU version which is free
for students and educators.

[Download CoppeliaSim Edu](https://www.coppeliarobotics.com/downloads) and
install for your OS. **Check that you have at least version 4.2.0**.

## Enable the remote API
In order to use the simulator one has to enable the API by copying the adequate
libraries that depend on the OS. The files should be copied into
`coppeliasim/remoteApiBindings`. Check the Matlab client in the [Official
documentation](https://www.coppeliarobotics.com/helpFiles/en/remoteApiClientSide.htm):
> To use the remote API functionality in your Matlab program, you will need
> following 3 items: 
> + `remoteApiProto.m` 
> + `remApi.m` 
> + `remoteApi.dll`, `remoteApi.dylib` or `remoteApi.so` (depending on your target platform)
>
> Above files are located in CoppeliaSim's installation directory, under
> programming/remoteApiBindings/matlab. You might have to build the remoteApi
> shared library yourself (using remoteApiSharedLib.vcproj or
> remoteApiSharedLib_Makefile) if not already built. 
>
> [...]
>
> Make sure your Matlab uses the same bit-architecture as the remoteApi
> library: 64bit Matlab with 32bit remoteApi library will not work, and
> vice-versa!

Additionally, some example paths have been provided on where this files are
located in a Windows machine.

Example path for `remApi.m` and `remoteApiProto.m`
```
C:\Program Files\CoppeliaRobotics\CoppeliaSimEdu\programming\remoteApiBindings\matlab\matlab
```

Example path for `remoteApi.dll`
```
C:\Program Files\CoppeliaRobotics\CoppeliaSimEdu\programming\remoteApiBindings\lib\lib\Windows
```

**Remember, copy these files into `coppeliasim/remoteApiBindings`.**

## Open the provided scene
Make sure to open [robot.ttt](./coppeliasim/robot.ttt) scene by using the menu
`File > Open Scene...` and navigating to where the `coppeliasim` folder located
in the root of the repository.