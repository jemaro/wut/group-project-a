function run_visualization(varargin)
    try
        delete(findall(0));
    catch
    end
    startup_lib;

    visualization(varargin{:})
end